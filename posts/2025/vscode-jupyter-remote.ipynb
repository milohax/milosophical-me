{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "b442ccd1-82ef-460a-81d3-75d2a2ac3e56",
   "metadata": {},
   "source": [
    "I'm experimenting with using Jupyter to capture Linux server commands in a Notebook while I figure out doing GitLab Geo setup. It's complicated, so I want to [keep good notes](/blog/2015/4-bit-rules-of-computing-part-1.html), but copy/paste from the terminal to a text editor is a chore I'd rather not deal with. Instead I would like to keep a literate-programming style notebook. I learnt recently that there is a [Jupyter kernel for running bash](https://pypi.org/project/bash_kernel/) instead of python, and that would be ideal.\n",
    "\n",
    "This post describes how to set up using Jupyter Notebooks on a remote host, and then running and editing notebooks using Visual Studio Code's support for Jupyter.\n",
    "\n",
    "You can _also do this without VSCode_, by SSH tunnelling a local web browser to the host. That would be better in cases where users' `/home` volume size is restricted. I briefly discuss this too."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "8d8d674c",
   "metadata": {},
   "source": [
    "<!-- TEASER_END -->\n",
    "\n",
    "---"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "cf6b4dac-ff32-4cc2-9fa4-bc9a5ea9cb7c",
   "metadata": {},
   "source": [
    "## Install Python, Jupyter, and the Bash kernel\n",
    "\n",
    "First we need an environment for running Jupyter. I did try this globally, since I'm using a dedicated host, but it works better as a virtual environment (\"venv\").\n",
    "\n",
    "This server runs Ubuntu, so the software package installation is specific to that OS."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "1e0ed092-df7c-44e8-a591-fab4f7630d6f",
   "metadata": {
    "vscode": {
     "languageId": "plaintext"
    }
   },
   "source": [
    "### Install the python-venv package\n",
    "\n",
    "In Ubuntu, Python is separated into multiple `.deb` packages, and the python virtual environment functions are in `pythonX.Y-venv`.  Don't worry about the specific version in the package name: if you install `python3-venv` then APT will get the latest version available:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "f9a4b95d-8e66-47bc-bd73-3ff3e7c09056",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Reading package lists... Done\n",
      "Building dependency tree       \n",
      "Reading state information... Done\n",
      "python3 is already the newest version (3.8.2-0ubuntu2).\n",
      "The following NEW packages will be installed:\n",
      "  python3-venv\n",
      "0 upgraded, 1 newly installed, 0 to remove and 5 not upgraded.\n",
      "Need to get 1228 B of archives.\n",
      "After this operation, 11.3 kB of additional disk space will be used.\n",
      "Do you want to continue? [Y/n] \n",
      "Get:1 http://us-west1.gce.archive.ubuntu.com/ubuntu focal/universe amd64 python3-venv amd64 3.8.2-0ubuntu2 [1228 B]\n",
      "Fetched 1228 B in 0s (17.2 kB/s)       \n",
      "Selecting previously unselected package python3-venv.\n",
      "(Reading database ... 197373 files and directories currently installed.)\n",
      "Preparing to unpack .../python3-venv_3.8.2-0ubuntu2_amd64.deb ...\n",
      "Unpacking python3-venv (3.8.2-0ubuntu2) ...\n",
      "Setting up python3-venv (3.8.2-0ubuntu2) ...\n",
      "Processing triggers for man-db (2.9.1-1) ...\n"
     ]
    }
   ],
   "source": [
    "sudo apt install python3 python3-venv"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d3b7767c-876a-4e13-98ba-0e6c580246ee",
   "metadata": {},
   "source": [
    "### Create a python virtual environment\n",
    "\n",
    "Make a directory for keeping your notebooks, and navigate to it:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "0b5f94d9-f1a2-4d87-966a-d9477f3d5545",
   "metadata": {},
   "outputs": [],
   "source": [
    "NB=~/lab/notebooks\n",
    "mkdir -p $NB; cd $NB"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d2e17dc4-ab26-499b-b0eb-3c8788529a7d",
   "metadata": {},
   "source": [
    "Create a virtual environment using Python's `venv` module, installed above. I'm following the usual practice of _putting the venv within the project directory_, rather than my preferred practice of putting venvs in `~/lib/venv`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "1f69ffcf-249e-4df4-88ef-8d0d696a8e11",
   "metadata": {},
   "outputs": [],
   "source": [
    "python3 -m venv venv"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "9813b3a7-0270-4c2d-ab77-7c39e988b04e",
   "metadata": {},
   "source": [
    "You can now activate the venv for the current shell session:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "4d426b30",
   "metadata": {
    "vscode": {
     "languageId": "shellscript"
    }
   },
   "outputs": [],
   "source": [
    "source venv/bin/activate"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "93d31979-5dda-4094-a50e-4c3ed9a20eda",
   "metadata": {},
   "source": [
    "## Install Jupyter and bash_kernel"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "9dd49d9d",
   "metadata": {},
   "source": [
    "Now that the venv is activated, install Jupyter and the bash_kernel into it:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "08a51718",
   "metadata": {
    "vscode": {
     "languageId": "shellscript"
    }
   },
   "outputs": [],
   "source": [
    "pip install -U pip jupyter bash_kernel\n",
    "python -m bash_kernel.install"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ab8aa5cd",
   "metadata": {},
   "source": [
    "At this point we can start Jupyter with the bash kernel."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "50d2659f",
   "metadata": {},
   "source": [
    "## Running Jupyter notebook\n",
    "\n",
    "Launch the notebook, without automatically spawning a web browser, since that makes no sense on the remote host:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "42a9704a",
   "metadata": {
    "vscode": {
     "languageId": "shellscript"
    }
   },
   "outputs": [],
   "source": [
    "jupyter notebook --no-browser"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a3c93e54",
   "metadata": {},
   "source": [
    "Jupyter will print status information to the terminal. Among it are details for connecting securely to the server to view and run notebooks:"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "18df8aa3",
   "metadata": {
    "vscode": {
     "languageId": "raw"
    }
   },
   "source": [
    "```\n",
    "\n",
    "[I 2025-03-08 21:30:42.929 ServerApp] Serving notebooks from local directory: /home/mjl/lab/notebooks\n",
    "[I 2025-03-08 21:30:42.929 ServerApp] Jupyter Server 2.14.2 is running at:\n",
    "[I 2025-03-08 21:30:42.929 ServerApp] http://localhost:8888/tree?token=REDACTEDTOKEN\n",
    "[I 2025-03-08 21:30:42.929 ServerApp]     http://127.0.0.1:8888/tree?token=REDACTEDTOKEN\n",
    "[I 2025-03-08 21:30:42.929 ServerApp] Use Control-C to stop this server and shut down all kernels (twice to skip confirmation).\n",
    "[C 2025-03-08 21:30:42.933 ServerApp]\n",
    "\n",
    "    To access the server, open this file in a browser:\n",
    "        file:///home/mjl/.local/share/jupyter/runtime/jpserver-7453-open.html\n",
    "    Or copy and paste one of these URLs:\n",
    "        http://localhost:8888/tree?token=REDACTEDTOKEN\n",
    "        http://127.0.0.1:8888/tree?token=REDACTEDTOKEN\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d37c4f62",
   "metadata": {},
   "source": [
    "Note that a fresh token is generated each time that Jupyter starts."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "4835db60",
   "metadata": {},
   "source": [
    "### Tunnel to Jupyter and open in a local web browser\n",
    "\n",
    "Typically, the next step when running Jupyter on a remote host, is to \"tunnel to the host\" and then connect a local web browser through the tunnel to view the notebook server. The SSH command for that is:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "88d6ed83",
   "metadata": {
    "vscode": {
     "languageId": "shellscript"
    }
   },
   "outputs": [],
   "source": [
    "ssh -N -L localhost:8888:localhost:8888 username@remote_host"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "bc77bce0",
   "metadata": {},
   "source": [
    "After that is connected, use your local web browser to view the URL printed by Jupyter above. If the tunnel held, you are in and have a web interface to Jupyter Notebook.\n",
    "\n",
    "### Running bash in a notebook\n",
    "\n",
    "You can create notebooks on the remote host using the Bash kernel, and run bash commands in cells of the notebook, executing on the remote host!\n",
    "\n",
    "What's great about this is that the bash kernal maintains state, just like any other Jupyter kernel. So the environment persists between cells, as it is the same process executing each cell, unlike with Python-kernel cell-magics which fork a sub-process to execute the shell commands.\n",
    "\n",
    "So this makes the bash kernel for Jupyter ideal for exploring and documenting complicated Linux server setup."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "09f176e2",
   "metadata": {},
   "source": [
    "## Connect to the host from VSCode\n",
    "\n",
    "A nicer way to do this is with [VSCode Remote SSH](https://code.visualstudio.com/docs/remote/ssh). It has these advantages:\n",
    "\n",
    "1. Automatically forward ports, so port `8888` will be browsable locally, without the SSH command above\n",
    "1. Nicer / more powerfull editing than Jupyer in a browser\n",
    "1. Open the notebook _directly_ and connect to the server running on the host\n",
    "1. Absolute filenames and directories that appear in the shell output can be clicked on and opened with VSCode\n",
    "1. View the Jupyter console output, and the notebook, together in the same window\n",
    "1. Navigate an outline of the notebook (like Jupyter Lab), browse files in the remote filesystem, run a remote terminal\n",
    "1. Other IDE features, like IntelliSense for shell aliases and functions _from within the Notebook_!\n",
    "\n",
    "If you followed along the above with SSH in a normal Terminal application, but now want to try using VSCode, then exit the Jupyter server with <kbd>⌃</kbd>-<kbd>C</kbd> (`Ctrl-C`), and open a new VSCode Remote SSH connection to the host, from VSCode:\n",
    "\n",
    "- <kbd>F1</kbd> `Remote-SSH: Connect Current Window to Host...`\n",
    "\n",
    "(If the host is in your SSH configuration, it'll be selectable from the quick-pick)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "0abc7d1e",
   "metadata": {},
   "source": [
    "### Install Python and Jupyter VSCode extensions\n",
    "\n",
    "You must install these VSCode extensions _in the Remote-SSH session_. They are necessary for the integration to work:\n",
    "\n",
    "- [Python](https://marketplace.visualstudio.com/items?itemName=ms-python.python) (Microsoft)\n",
    "- [Jupyter](https://marketplace.visualstudio.com/items?itemName=ms-python.python) (Microsoft)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "9778ba1e",
   "metadata": {},
   "source": [
    "### Launch Jupyter inside VSCode\n",
    "\n",
    "Having installed the extensions, we can now open a new notebook in VSCode.\n",
    "\n",
    "Open the VSCode integrated terminal and change to the directory with the notebooks:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "ce6ea2eb",
   "metadata": {
    "vscode": {
     "languageId": "shellscript"
    }
   },
   "outputs": [],
   "source": [
    "cd ~/lab/notebooks"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "75db2f3f",
   "metadata": {},
   "source": [
    "Make sure to activate the environment:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "69fc7103",
   "metadata": {
    "vscode": {
     "languageId": "shellscript"
    }
   },
   "outputs": [],
   "source": [
    "source venv/bin/activate"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "74744bf3",
   "metadata": {},
   "source": [
    "Then start Jupyter again, like before:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "555ecddc",
   "metadata": {
    "vscode": {
     "languageId": "shellscript"
    }
   },
   "outputs": [],
   "source": [
    "jupyter notebook --no-browser"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f2269617",
   "metadata": {},
   "source": [
    "The Jupyter server output will contain the new authentication token information, for this instance of Jupyter."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "05b4c624",
   "metadata": {},
   "source": [
    "## Making new notebooks and running cells\n",
    "\n",
    "With Jupyter running inside VSCode, there's an automatic port-forwarding for Jupyter.\n",
    "\n",
    "### Open a local web browser through the VSCode tunnel\n",
    "\n",
    " VSCode automatically detects listening TCP ports and tunnels them for us. We can connect our local web browser through the tunnel that VSCode provides, without need for special SSH parameters. Review the **PORTS** panel to confirm the forwarded ports, but Jupyter usually opens port `8888` by default:\n",
    "\n",
    "> http://localhost:8888/?token=PUT_TOKEN_HERE\n",
    "\n",
    "Use the connection details shown on the **TERMINAL** panel for the token.\n",
    "\n",
    "### Open a Notebook directly in VSCode\n",
    "\n",
    "You can create and interact with notebooks directly in VSCode:\n",
    "\n",
    "1. Create a new buffer.\n",
    "1. Save the buffer with a `.ipynb` filename extension. The file re-loads as a Notebook, in [VSCode's Notebook editor](https://code.visualstudio.com/docs/datascience/jupyter-notebooks).\n",
    "1. Select the Bash kernel from the **Select a kernel** button in the top-right.\n",
    "    1. Choose **Jupyter kernel...** from the quick-pick list.\n",
    "    1. Then choose **Bash (venv/bin/python)**.\n",
    "1. Add a code cell by pressing <kbd>A</kbd> and enter some bash code in it. Try `uname -a` to convince yourself it is the remote host.\n",
    "1. Run the cell to test that it works, using the  <kbd>⇧</kbd>-<kbd>return</kbd> keyboard shortcut.\n",
    "\n",
    "Enjoy editing and executing bash cells with the full features of the VSCode editor and any other extensions that you would like to add."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "0f403d06",
   "metadata": {},
   "source": [
    "# Side-note: Storage volume consumption by VSCode and Jupyter\n",
    "\n",
    "Do note that after installing all of this, there will be quite a lot of data in your `$HOME` directory. The VSCode server components added 510MB:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "d54d7a19",
   "metadata": {
    "vscode": {
     "languageId": "shellscript"
    }
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "21M\t/home/mjl/.vscode-server/code-e54c774e0add60467559eb0d1e229c6452cf8447\n",
      "60M\t/home/mjl/.vscode-server/data\n",
      "195M\t/home/mjl/.vscode-server/cli\n",
      "234M\t/home/mjl/.vscode-server/extensions\n"
     ]
    }
   ],
   "source": [
    "du -sh ~/.vscode-server/* | sort -h"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "85453d00",
   "metadata": {
    "vscode": {
     "languageId": "shellscript"
    }
   },
   "source": [
    "Breaking down the 234MB of extensions:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 21,
   "id": "eccfe2cf",
   "metadata": {
    "vscode": {
     "languageId": "shellscript"
    }
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "8.0K\t/home/mjl/.vscode-server/extensions/extensions.json\n",
      "116K\t/home/mjl/.vscode-server/extensions/ms-toolsai.jupyter-keymap-1.1.2\n",
      "512K\t/home/mjl/.vscode-server/extensions/ms-toolsai.vscode-jupyter-slideshow-0.1.6\n",
      "556K\t/home/mjl/.vscode-server/extensions/ms-toolsai.vscode-jupyter-cell-tags-0.1.9\n",
      "21M\t/home/mjl/.vscode-server/extensions/ms-toolsai.jupyter-2025.1.0-linux-x64\n",
      "25M\t/home/mjl/.vscode-server/extensions/ms-toolsai.jupyter-renderers-1.1.0\n",
      "38M\t/home/mjl/.vscode-server/extensions/ms-python.debugpy-2025.4.0-linux-x64\n",
      "48M\t/home/mjl/.vscode-server/extensions/ms-python.python-2025.2.0-linux-x64\n",
      "103M\t/home/mjl/.vscode-server/extensions/ms-python.vscode-pylance-2025.3.1\n"
     ]
    }
   ],
   "source": [
    "du -sh ~/.vscode-server/extensions/* | sort -h"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "c3b87007",
   "metadata": {},
   "source": [
    "Also the python virtual environment, and Jupyter itself, are pretty significant:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 22,
   "id": "58bbfb19",
   "metadata": {
    "vscode": {
     "languageId": "shellscript"
    }
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "247M\t/home/mjl/lab/notebooks/venv\n"
     ]
    }
   ],
   "source": [
    "du -sh ~/lab/notebooks/venv"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "25e5ed85",
   "metadata": {
    "vscode": {
     "languageId": "shellscript"
    }
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "12K\t/home/mjl/lab/notebooks/venv/share/applications\n",
      "12K\t/home/mjl/lab/notebooks/venv/share/man\n",
      "40K\t/home/mjl/lab/notebooks/venv/share/icons\n",
      "2.2M\t/home/mjl/lab/notebooks/venv/share/python-wheels\n",
      "23M\t/home/mjl/lab/notebooks/venv/share/jupyter\n"
     ]
    }
   ],
   "source": [
    "$ du -sh ~/lab/notebooks/venv/share/* | sort -h"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "55b57dfc",
   "metadata": {},
   "source": [
    "Be mindful of this storage consumption!  Running Jupyter within VSCode on remote hosts is probably something only to be done in environments where you can afford this extra &frac34; GB storage.\n",
    "\n",
    "It's also installed _per user_, so a shared host — such as a university mainframe — will quickly balloon the `/home` volume."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Bash",
   "language": "bash",
   "name": "bash"
  },
  "language_info": {
   "codemirror_mode": "shell",
   "file_extension": ".sh",
   "mimetype": "text/x-sh",
   "name": "bash"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
