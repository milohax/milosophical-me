<!--
.. title: Code Clubbing 2023
.. slug: codeclub-0
.. date: 2023-03-18 09:21:30 UTC+11:00
.. tags: getkidscoding, CodeKids, 
.. category: 
.. link: 
.. description: 
.. type: text
-->

I revived my Code Club for 2023, here's a quick report on the first day.

<!-- TEASER_END -->

---

My wife was chatting with the school principal and encouraged her to contact me about running another [Code Club](https://codeclubau.org/) in the school. Now that adult helpers are allowed back on school grounds, and I have a new job which lets me work from home, and also put "family and friends first" and supports volunteering, I have all the opportunity to come to school and run a club.

The school arranged for about 16 children (including my youngest son) to be in the club, which will meet on Fridays between 1PM and 2PM. The principal is also attending to facilitate, and is keen to join in too! The group is a mix of years 4 and 5 from different classes, mainly boys, though we will work to be more diverse and inclusive, encouraging more girls.

I did do a quick [refresher course](https://www.openlearning.com/codeclubau/courses/codeclubintheclassroom2023/homepage/?cl=1) to review how to run a club, support kids and be safe, and this also gave me some ideas for introducing myself and what we'll do in the club.

I also did some of the exercises in [Scratch](https://scratch.mit.edu/) to remind myself how to use it. I was pleasantly suprised by some neat improvements in the user interface, the best (that I noticed) being that the _Move_ blocks will change their default `x` and `y` coordinates to match the current Sprite's position. It's much easier to enter in coordinates for blocks such as `go to x, y` and `glide z seconds to x, y`.

## In the classroom

In the classroom, there was already equipment set up! I was surprised that the school has a couple dozen laptops all set up and connected to the Internet, and the kids already knew all about how to log in on them, start browsers, and also to plug them into the charging stations and put them away at the end. So much better (and easier) than when I did this years ago. Also the equipment is up to the task, not slow and frustrating to use. I felt like I was in a my own computer classroom from the 1980s, where everyone had a computer, knew how to turn it on and were ready to learn something.

We started with laptops closed and talked about what we're going to do in the club. The difference between using computers, and creating new programs. About half the class had already some exposure to programming with block languages such as Scratch or Blockly. One boy commented that he gets frustrated by making the computer do what he wants and having to be very precise, which seguayed nicely into "I am a robot"...

I asked one of the boys to go to the back of the room, collect a post-it that I'd put there beforehand, and bring it back to their desk. After commenting how easy that was to direct and follow, I announced that I am a robot, and asked the kids to direct me to do the same with the other post-it.

The kids enjoyed getting me do do this for about 5 minutes, while I walked into chairs and tables, the door, spun around a lot... it was a great suggestion from the training I did earlier in the week. I think the principal was bemused but she seemed delighted too. I'm such a clown.

## Setting up computers

We got down to laptops after that, and it was bedlam like you'd expect, but a lot of fun! By the end we had everyone signed up with a Scratch account linked to their school email addresses or ones with parent supervision. A few had remixed [my catch the bus remix](https://scratch.mit.edu/projects/820363590) from the coaching and done some changes, like making the bus go the other way, or removing the hippo (and discovering the little `broadcast 'made it'` which I'd used there). One even stayed to show me what they made.

## Planning the future

I asked for feedback and to measure interest in what they would like to do in the club:

- There was a lot of interest in Minecraft coding
- Some are interested in doing animations
- Many can draw and would like to draw their own sprites
- Some would like to do games
- Some were interested in learning Python, which will be a gateway to more advanced things
- There is a history class this term which some are interested to turn into an animated story

So, a lot to look forward to!

Next week we'll start on [the introduction to Scratch learning pathway on RaspberryPi.org](https://projects.raspberrypi.org/en/pathways/scratch-intro) because there are a mix of skills. We may stay on the path, or we may stray to anoter, depending how we go. While the early projects might be easy for those who've done this before, I think there is big scope for those to learn some leadership as well.

At the end we all packed up and I even got some un-prompted thank-you's. I'm excited to go back next week.
