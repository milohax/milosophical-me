<!--
.. title: Tech projects at start of 2023
.. slug: hackng-2023
.. date: 2023-01-22 17:51:08 UTC+11:00
.. tags: projects, blogging, experiments, hacking
.. category: 
.. link: 
.. description: Brain dump of all the tech projects I'm working on and want to complete soon.
.. type: text
-->

I had a thought about what's on my wish-list for tech tasks that I'd like to complete this year, and then it struck me that there's a lot going on to update my blog with.

<!-- TEASER_END -->

---

The stuff I've been working on lately, but not blogging about, is this:

 - Getting my [bootstrap](https://gitlab.com/milohax-net/radix/bootstrap/-/issues) to work on macOS and openSUSE (testing in Tumbleweed) (works on Mac, openSUSE is 90% complete)
 - Ansible playbook for [workstation](https://gitlab.com/milohax/workstation/) setup (works for Mac! Now need to add a branch for SUSE)
 - Figuring out how I want to [install my dotfiles](https://gitlab.com/milohax-net/radix/dotfiles/-/issues/4) (I am leaning towards [a bare Git Repo](https://www.atlassian.com/git/tutorials/dotfiles))
 - Playing with [tmux](https://gitlab.com/milohax-net/radix/dotfiles/-/issues/3)
 - Switching to [mastodon](https://hachyderm.io/@milohax)
 - Getting ready for a short (15 minute) demo at the [next virtual GitLab Meetup](https://www.meetup.com/gitlabanz/events/290508374/)

I'm all over the place!

## GitLab Meetup

The thing that's most urgent at the moment is **actually preparing for that Meetup**. It's a virtual meetup, happening at 12:30PM Melbourne time on Tuesday February 14<sup>th</sup> (2023-02-14 01:30 UTC). My part is very small, and I'm going to be doing a short demo based upon [my GitLab Profile blog posts](/tags/gitlab-profile.html). The key here will be to keep it short and sweet. I do want to accomplish some key things:

 - Do it on a [self-managed GitLab CE](https://gitlab.com/mlockhart/lab/-/issues/166) rather than gitlab.com (because the meetup seems to be talking about CI/CD in CE)
 - Explain the [custom profile project](https://docs.gitlab.com/ee/user/profile/#add-details-to-your-profile-with-a-readme) naming convention
 - Show updating a git repo from within a CI job (and [link to my code for reference](https://gitlab.com/milohax/hax/-/blob/9910fed0dd38fd523f0f1a98fc6f9998326cf334/gitlab/git_clone_and_push.yml))
 - Quickly demonstrate [building and publishing](https://gitlab.com/milohax/hax/-/snippets/2220897) a [custom docker image](https://gitlab.com/milohax/hax/container_registry/2563226) with the tools needed, and [overcoming a common access problem](https://gitlab.com/milohax/hax/-/snippets/2220896)
 - Keep the actual mechanics of RSS parsing very simple: the point is to demo GitLab, not the RSS parsing, although the examples use it
 - I also want to make sure that I'm following [GitLab's Handbook on public speeking](https://about.gitlab.com/handbook/marketing/corporate-communications/speaking-resources/)

That's a lot just in itself, actually! You can see that I've been busy documenting all this in issues and snippets. I _only_ need to pull it all together into something that can be quickly groked by someone else&hellip;.

## Switching to Mastodon

I've joined the recent exodus from the bird app. The latest round of layoffs included those who do the mediation of toxic tweets. That was the final straw for me, though I've had an on/off relationship for a few years now.  My new handle is [@milohax@hachyderm.io](https://hachyderm.io/@milohax). I've started tooting there and I feel about ten times more comfortable. I've started making connections around technical topics, which is that server's speciality. I also have an alt for my Australian interests: [@milohax@mastodon.au](https://mastodon.au/@milohax), though I'm not very active there yet. I'll see how it goes having an alt, I'm not sure if it's the right thing to do or not.

So, all the links in this blog that went to the bird now go to Hachyderm, and from there to the rest of the fediverse. It's so much nicer and fixes a lot of the problems that have made centralised social media so toxic. As more of the people I follow make the same move, I'll need to visit those places less and less. I'm looking forward to it.

## Booting up a new workstation

My work upgraded my MacBook Pro to a new model with an M1-Max chip, and I got to keep the 2019 MacIntel MBP! It's very nice to have _my own Macintosh_ after 38 years, and about 25 years after doing my major dev project at UTas on a [Macintosh LC II](https://everymac.com/systems/apple/mac_lc/specs/mac_lc_ii.html). I do like the Mac, even though moving back to one in 2016 was an adjustment. While I miss using KDE as my daily driver, macOS does have some very nice creature comforts, just ones that I wouldn't have paid for myself. Now I don't need to, so I'm looking into configuration management.

I also want to keep configurations for my Linux desktop, which runs KDE on openSUSE, the [best Linux for devolopment work, IMO](https://milohax.net/#OpenSUSE).

So to do this, I'm updating my [dotfiles](https://gitlab.com/milohax-net/radix/dotfiles), and writing a new [ansible workstation playbook](https://gitlab.com/milohax/workstation) to install software packages for macOS and openSUSE. It's based off [Jeff Geerling's mac-dev-playbook](https://github.com/geerlingguy/mac-dev-playbook). To install the base requirements for Ansible, I'm updating a [bootstrap](https://gitlab.com/milohax-net/radix/bootstrap) project, which also clones my [bash library](https://gitlab.com/milohax-net/radix/bashing) and [fish library](https://gitlab.com/milohax-net/radix/fishing). That's a whole lot of things to pull together, but I'm working through it gradually. Some of this effort will translate to my work as well, as I'd like to put my dotfiles and bash library onto test servers using Ansible.

## Tmux is really nice

I have read about `tmux` in the past, and it looked really good, but I never had opportunity to use it at my last job. For this job though, I am routinely running up temporary High Availability GitLab deployments to test customer scenarios. A modest-sized [3k reference deployment](https://docs.gitlab.com/ee/administration/reference_architectures/3k_users.html) installs with 32 hosts using our [GitLab Environment Toolkit](https://gitlab.com/gitlab-org/gitlab-environment-toolkit) (the same size as a banking application I once supported!) and this is ideal for `tmux`'s windows and panes, and synchronized typing. At the moment it's a new hobby, but it will give me some material to write about.

## Blogging

Last year I only made one post. I'd like to make more, though I'm aware that I've made promisses here before `;-)`

My wish is to be semi regular, and to federate my posts on [Dev.to](https://dev.to/sinewalker) and the fediverse. It least I know that I have a lot to write!
