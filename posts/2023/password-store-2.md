<!--
.. title: Password databases: updating master key
.. slug: password-store-2
.. date: 2023-05-28 20:58:56 UTC+10:00
.. tags: password-store, passwords, openkeychain, gpg
.. category: 
.. link: 
.. description: Updating a pass master encryption key after expiry
.. type: text
-->

It's been five years since I created my Unix password store, encypted with my GPG key pair `password-store`. That key expired and now I need to update my database to a new master key.

Now is also a good time to clear out the git history. My key has not (to my knowledge) been compromised, so I don't need to remove all instances of passwords encrypted with the old key, but it will be nice to clear out the old history.

<!-- TEASER_END -->

---

Here are the steps I performed to generate a new key pair and reencrypt the password store with it.

## Generate a new key

I created a new key without an expiry date. This is maybe controversial, but I don't intend to _share_ the key with anyone, so I feel it's better to not have to do this again unless the key is compromised.

To do this, I ran:

`gpg --full-generate-key`

Then answered `0` to _Please specfy how long the key should be valid_. This means it does not expire at all. 

It's necessary to give the new key a different name to the old one, so that I can have both of them on my key chain together when reencrypting. I went with `passwords-master` as as the "real name".

Following generating new key, I saved it and shared it to my other devices as [explained in 2018](/blog/2018/password-store-1.html).

## Reencrypt password store

I followed these steps to remove history and reencrypt.

1. Remove the current `~/.password-store` directory
1. Shallow clone from GitLab:

    ```
    git clone --depth 1 git@gitlab.com:milohax/pass.git .password-store
    ```

1. Reencrypt by specifying the new `passwords-master` gpg-id:

    ```
    pass init passwords-master
    ```

    This prompts for the _old_ key's passphrase to decrypt. It will note the key ID of the key it's reencrypting with.

1. Verify by decripting a password. This will prompt for the passphrase of the _new_ key

1. Force-push to GitLab:

    ```
    pass git push --force origin master
    ```

## Update on other machines

Each other machine needs to do a fresh pull, and import the new key onto the device's key chain. This was spelled out in the earlier post, but here are the high-level steps and commands.

1. Export the _public_ key:

    ```
    gpg --export --armour passwords-master > passwords-master.sec.asc
    ```

1. Then, append the _private_ key:

    ```
    gpg --export-secret-keys --armour passwords-master >> passwords-master.sec.asc
    ```

1. Don't forget to _encrypt_ the ASCII file with the device key if you're sending by email:

    ```
    gpg --trust-model always -e -r milo-nokia6.1 passwords-master.sec.asc
    ```

1. Then send the `.gpg` file, not the `.asc`

With the new key on the keychain, and the updated password-store, you can now view a password entering the new key's passphrase.