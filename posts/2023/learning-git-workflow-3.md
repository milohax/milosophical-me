<!--
.. title: Learning DVCS Workflow - 3
.. slug: learning-git-workflow-3
.. date: 2023-03-01 18:45:28 UTC+11:00
.. tags: git, git-lfs, gitlab, svn, subversion
.. category: 
.. link: 
.. description: Migrating SVN to git when the repo contains big files
.. type: text
-->


Recently for work I wrote out some steps to work through to get from a Subverison (SVN) repository containing large files, into a git repository using git Large File Storage (LFS) to house the big files separately.

<!-- TEASER_END -->

---

A customer had used [git svn](https://git-scm.com/docs/git-svn) to export a Subversion repository into a local git repository, and then, after adding their GitLab project as the remote origin, attempted to upload this to gitlab.com with:

```sh
git push -u origin --all
```

It failed with [HTTP 413](https://httpcat.us/413) because of a 5GB max transfer limit. There is a very large file from the SVN repository! One solution is to move these large files into LFS, which bypasses this limit.

It may help to understand this if you read about LFS, what it does, and how it works. While  the [announcement blog post from 2017](https://about.gitlab.com/blog/2017/01/30/getting-started-with-git-lfs-tutorial/) explains what LFS is and why git (and also GitLab/GitHub/Bitbucket and so on) use it, [GitLab's documentation](https://docs.gitlab.com/ee/topics/git/lfs/#git-large-file-storage-lfs) is already fairly technical. The quick summary is:

* Git LFS is a technology to overcome `git`'s shortcomings for working with large files. I also covered some of this in [Learning DVCS Workflow - 2](/blog/2021/learning-git-workflow-2.html)
* It is an extension to git (much like `git-svn` is)
* It works by "tracking" the "large file types" in a storage that is separate to the normal "blob" storage used by `git` to track changes
* From then on, changes to these tracked files are treated by copying the whole file and not attempting to do `diff` processing on them. Instead `git` tracks the different versions with small "_pointers_" into the LFS
* The main advantage of LFS is to improve `git clone` speed by only cloning the _latest_ version of LFS files, and getting older versions only on request, by following the pointers
* In GitLab, the 5GB file transfer limit is lifted for LFS files

### Migrating SVN to git

Follow these high-level steps:

1. Convert the SVN repository into a new git repository (see [the manual for git svn](https://git-scm.com/docs/git-svn))
1. On gitlab.com, create a new, _empty_ project to push into
1. Add the GitLab project as the `origin` remote
1. Make sure to [enable LFS in the project's settings](https://docs.gitlab.com/ee/user/project/settings/index.html#configure-project-visibility-features-and-permissions)
1. Install the [Git LFS client](https://git-lfs.com/) on your workstation(s)
1. (the tricky part) locate the large files which came from SVN
1. Use [git-lfs-migrate](https://github.com/git-lfs/git-lfs/blob/main/docs/man/git-lfs-migrate.adoc) to migrate the large files _and their history_, over to git LFS
1. `git push -u origin --all`

Step 7 to use `git lfs migrate` is usually discussed in git literature as a "history rewrite", which can cause disruption in shared git workflows, but since we are migrating from Subversion, and no-one shares the git repository yet, we do not need to be concerned. It would be good to get all this migration done early though, to avoid potential trouble in the future.

Let's look at steps 5, 6 and 7 in detail.

### Locate large files which came from SVN

This [Stack Overflow answer](https://stackoverflow.com/a/42544963/776953) has a nice shell one-liner that uses `git` to list all the files in a repository, sorted by size:

```sh
git rev-list --objects --all |
  git cat-file --batch-check='%(objecttype) %(objectname) %(objectsize) %(rest)' |
  sed -n 's/^blob //p' |
  sort --numeric-sort --key=2 |
  cut -c 1-12,41- |
  $(command -v gnumfmt || echo numfmt) --field=2 --to=iec-i --suffix=B --padding=7 --round=nearest
```

Use it to see which are the big files. There may be a pattern in the files it finds, such as maybe media files (with names ending in `.jgp`, `.wav`, `.mp4`, or `.png`).

Use this to build a list of "file types" to track, then tell `git lfs` about them.  First, let's install the `lfs` extension:

```sh
git lfs install
```

This only needs to be done _once per workstation_, but all developers sharing the new git repo will need it, along with the `git` command line tool.

Now, tell `git lfs` to track the file types you found.  For instance, this tracks common image formats and compressed files:

```sh
git lfs track "*.jpg" "*.jpeg" "*.gif" "*.png" "*.zip" "*.gz"  
```

The `git lfs track` will make a new `.gitattributes` that is used for future commits involving these types of files. Add that file and commit it now:

```sh
git add .gitattributes
git commit -m "Track large binary files in LFS"
```

This doesn't move the files into LFS though, that is the next step.

### Migrate the tracked files' history

Having identified which files need to be moved, you can use `git lfs migrate` to move them.

Dry run of the migration. Use the option `--everything` to perform a migration in all branches. Use `--include` option to only migrate the files identified for git lfs track.

```sh
git lfs migrate info --everything --include="*.jpg,*.jpeg,*.gif,*.png,*.zip,*.gz"
```

This shows how the files' history will be changed to put them into LFS. If this checks out, perform the migration:

```sh
git lfs migrate import --everything --include="*.jpg,*.jpeg,*.gif,*.png,*.zip,*.gz"
```
Now when pushing to GitLab the large files will bypass the 5GB restriction and go to LFS storage. Also because they are tracked in the `.gitattributes`, any changes, or new files matching the patterns, will also go to LFS.

