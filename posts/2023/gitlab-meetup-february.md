<!--
.. title: GitLab Community Meetup February
.. slug: gitlab-meetup-february
.. date: 2023-03-02 21:31:19 UTC+11:00
.. tags: projects, blogging, rss, ruby, gitlab, cicd
.. category: 
.. link: 
.. description: Script from my presentation at the first GitLab Australia/New Zealand meetup in Febrpary 2023
.. type: text
-->

These are the notes for a [video presentation I made](https://www.youtube.com/watch?v=iCj4esdQDts&list=UULFMtZ0sc1HHNtGGWZFDRTh5A&index=39) for the February 2023 GitLab Australia/New Zealand community meetup. The meetup was organised by [Alec Clews](https://gitlab.com/alecthegeek) and he invited me to present.

You can view [the presentation slides](https://docs.google.com/presentation/d/e/2PACX-1vSy5Bdz5MrddZOrfzwAvi3pV7QO1QLfnZ-TEvoUd8btlEtr7Ndoniok4kQKBn34sgyKmAuag4-DC9Jm/pub?start=false&loop=false&delayms=3000#slide=id.g12b319f6181_0_0) as well.

<!-- TEASER_END -->

---

I did the presentation live, but we didn't record it, so I made the video afterwards from the notes I had prepared earlier, and demonstrating on a test GitLab CE instance. I recorded the video in one take because I don't have any editing skills, and I drifted off the script a little at the end. This is what I had though, so it's going on the blog as-is.

I'd like to rework this a little and post it again without glossing over the setup I'd done for the containers, but it's basically what I had blogged about earlier.

<p style="text-align: center;">
<iframe width="640" height="480" src="https://www.youtube.com/embed/iCj4esdQDts" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
</p>

## Introduction

I'm Mike and I work as a Support Engineer for GitLab in Tasmania. I have been in this role since April 2020, right around when GitLab 12.10 was released, and in this time I've had lots of exciting opportunities helping large and small customers to use GitLab. The GitLab Support team is global and comprises incredibly talented engineers who excel at identifying nuanced problems in large environments built and maintained by others.

## Updating GitLab profile with blog posts

GitLab profile READMEs are great. I also write blog posts that I want to highlight there. Updating my profile anually every time after publishing a blog post is boring. What if I could automate this with GitLab CI/CD?

## What you need

- A GitLab account (either on a self-managed GitLab, or gitlab.com)
- A Personal Access Token on the account, with write-repository scope
- A GitLab Runner, or some spare CI minutes on gitlab.com's shared runners
- A Container image with `git` and `rsync` installed and a language runtime
- The URL of a blog or mastodon feed, or other RSS or Atom feed to read from
- Some code to read syndication feeds and a runtime for it

## Create a profile project

If you don't already have a profile README for your GitLab account, it's easy to create one. All you need is a new project with the same name as your username. For instance, mine work one is https://gitlab.com/mlockhart/mlockhart. For this presentation, I've created a sample README on a GitLab CE instance: https://gitlab-ce-suse.milohax.net/mjl/mjl

### Add a README (choose a format, I'm going with Markdown)

In this project, you add a README file. It can be any format which GitLab can understand, surch as AsciiDoc, Org-Mode, or reStructuredText. I'm going with Markdown since that is what I use every day, so I created `README.md`. It has a heading, some links to other sites in a bullet list, and a section to place my blog posts, between HTML comments (which are valid comments in Markdown)

```markdown
<!---BEGIN BLOGS--->
<!---END BLOGS--->
```
The idea here is to have a script that will do a glob substituton for these markers, and insert links to the blog posts that it reads from the blog's RSS.

### Review profile with the README

Once there is a specially named project with a README file, the file is added to your GitLab profile page, between the contributions grid and the Activity and Personal projects lists. Review the profile and make sure that the README is displayed. It won't have your feed yet, that's coming with the CI.

## Side-bar: Reading syndication feeds for blogs/issues/toots

Most blogging software, as well as certain kinds of GitLab activity, or Mastodon toots, will generate an Atom Syndication XML file that can be read by syndication software, such as Miniflux, or Feedly. Every time there you post a new item, the file is updated. This is a nice way to collect posts from accross different blogs or news sites, without you having to visit the different sites, or fill your email inbox with newsletters.

There are two standards for these syndication feeds. RSS (really simple syndication) is a 2002 World Wide Web Consortium recomendation. Atom is a 2005 internet standard (RFC 4287). There are free code libraries to read these files in most programming languages. I'm learning to program in Ruby, and I used Ruby's built-in feed reader module to do this. The module is called `RSS` after the older standard, but it can read both RSS and Atom feeds, and the `RSS::Parser` class automatically works with either feed format.

The full mechanics of this aren't terribly important for this demo, you could use another language to do the same thing. You can see my example script at

## Create a CI/CD .gitlab-ci.yaml

The script needs to make a change to your README file from within the CI. I have written a CI/CD module which does this in a generic way, called `git_clone_and_push.yml`. We'll include it in the profile's CI and then explain it later.

### Import git-cloan-and-push


### First run

DEMO

### Explaining the git-clone-and-push function

## Call to action

Add some posts from your company blog to your gitlab profile, or use gitlab's

