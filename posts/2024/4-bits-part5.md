<!--
.. title: 4-bit Rules of Computing, Part 5
.. slug: 4-bits-part5
.. date: 2024-12-29 09:29:16 UTC+11:00
.. tags: 4-bit-rules, tip, deprecation, bugs, engineering
.. category:
.. link:
.. description: Mike's 4-bit rules explained, part 5: deprecation and breaking changes
.. type: text
-->

A software team once changed their version control system's API, removing what they saw as redundant legacy functionality. What they didn't realize was that this particular piece of "technical debt" was actually a load-bearing wall in a customer's factory automation pipeline. The result? A business losing thousands of dollars per hour while the production line stood idle.

In software development, every API endpoint, command-line interface, or file format becomes part of someone's workflow, automation, and daily operations. When Microsoft introduced the Ribbon interface in Office 2007, they broke a promise that millions of users had built their muscle memory around. When a content management system I worked with removed its multimedia MIME-type feature in the name of code cleanup, it left a trail of broken websites and frustrated users in its wake.

We need to carefully balance between necessary changes and breaking changes, which brings me to Rule A: "Fix Mistakes, don't Break Promises". When does a "mistake" become serious enough to justify breaking a "promise"? How do we make necessary changes without leaving our users stranded?

This is the sixth part of my [blog series](/tags/4-bit-rules.html) expanding on my
[4-bit rules of computing](/pg/4-bit-rules.html).

<!-- TEASER_END -->

---

# **Rule A**: Fix Mistakes, don't break Promises

## Two Forces of Change

Software changes for two main reasons. First, our understanding of user needs evolves. When Microsoft introduced the Ribbon interface, it wasn't change for change's sake — it was a response to users struggling with discovering features buried in nested menus. The change aimed to make functionality more discoverable and accessible.

Second, is the pressure of technical debt. Developers face mounting costs maintaining legacy systems, supporting old formats, or working around historical design decisions. That MIME-type feature wasn't removed on a whim — it likely represented a maintenance burden, perhaps even a security risk.


### Strategies for Keeping Technical Promises

One could take the approach that both Git and IBM's REST APIs follow: _**never break existing behavior, only add new alternatives**_. In Git, your ancient deployment script using `git push -f` still works, even though they've introduced `--force-with-lease` as a safer option. Similarly, IBM's APIs [maintain old fields and endpoints while adding new ones][1][^1] that provide better functionality.

This is an ideal, but it may not be pragmatic to always follow this doctrine. Modern software projects have developed several patterns for managing necessary changes:

#### Collaborative Exploration

Before making widespread changes, invite customers to explore new approaches alongside developers. This creates a partnership where both sides learn: developers understand real-world usage patterns better, while users help shape solutions that work for their needs. While Microsoft's Ribbon interface change was done in consultation with a UX team and test users, it shows how disruptive sudden changes can be to a broader audience. Their approach with TypeScript demonstrates a better way. TypeScript [evolves through a public proposal process][2][^2], working with framework authors and the broader community before implementing major language features. This ensures TypeScript meets changing needs while maintaining compatibility.

#### Automated Migration Paths

Don't just tell users how to move to new methods: _help them do it_. Tools can analyze existing code or configurations and automatically update them to use new patterns. React provides [automated codemods][3][^3] for version upgrades, helping developers [automatically update component syntax and lifecycle methods][4][^4]. The key is transparency — users should understand and approve these migrations, not just have them happen silently.

#### Feature Flags and Gradual Rollout

Rather than forcing changes on all users simultaneously, [feature flags let us roll out changes gradually][5][^5]. Users or organizations can opt in when they're ready, testing the waters before committing to new approaches. Web browsers demonstrate this well — both [Chrome][6][^6] and [Firefox][7][^7] use _origin trials_ to let developers test upcoming web platform features before they become standard, while users can experiment with new features through browser settings.

#### Parallel Systems

When a change is too fundamental for feature flags, we can run old and new systems in parallel. The [Python 2-to-3 transition][8][^8] initially faced challenges by forcing an either-or choice. The community later succeeded by developing tools that let codebases run on both versions simultaneously, allowing gradual, file-by-file migration.

#### Smart Defaults with Configuration

Sometimes we can update default behavior while letting users retain old behavior through configuration. The [GNU Compiler Collection][9][^9] does this well — newer versions default to stricter warning flags and newer language standards, but you can always specify older standards if needed. Your old Makefiles still work, but new projects get better safety checks automatically. This is like an opposite of the Git/IBM ideal, but it may be a better option depending on the user base and the nature of the change.

#### Multi-Phase Deprecation

When we must remove functionality:

1. First, mark it as **deprecated** but keep it working
2. Add warnings when the deprecated functionality is used
3. Provide clear documentation about alternatives, and _a migration path_
4. Only remove it after several release cycles, when usage metrics show minimal impact

The key observation across all these strategies is this: _**the cost of change should fall on the software makers, not the consumers**_. Our customers trust us — breaking that trust should never be done lightly. Don't remove a feature unless and until there is an alternative, and people have migrated to it.


## Owning trust in personal relationships

These technical practices reflect values I try to apply more broadly: doing what I say I will, acknowledging mistakes when they happen, and working to make things right. I'm not perfect at this, but I've found that consistent behavior and honest accountability build better relationships, whether in code or in life.

When things must change, communicate it, and work together to come to mutually acceptable compromise.


[1]: https://www.ibm.com/docs/en/security-verify?topic=apis-api-compatibility-policy-deprecation-policies
[^1]: [IBM API Compatibility/deperecation policy (archive)](https://web.archive.org/web/20240430083538/https://www.ibm.com/docs/en/security-verify?topic=apis-api-compatibility-policy-deprecation-policies)

[2]: https://github.com/microsoft/TypeScript/wiki/Contributing-to-TypeScript#typescript-evolution
[^2]:[Microsoft: Contributing to TypeScript (archive)](https://web.archive.org/web/20241005213237/https://github.com/microsoft/TypeScript/wiki/Contributing-to-TypeScript#typescript-evolution)

[3]: https://github.com/reactjs/react-codemod/blob/master/README.md
[^3]: [React Codemods (permalink)](https://github.com/reactjs/react-codemod/blob/5207d594fad6f8b39c51fd7edd2bcb51047dc872/README.md)

[4]: https://legacy.reactjs.org/blog/2022/03/08/react-18-upgrade-guide.html#automatic-batching
[^4]: [React-JS 18 upgrade guide (archive)](https://web.archive.org/web/20241130161350/https://legacy.reactjs.org/blog/2022/03/08/react-18-upgrade-guide.html#automatic-batching)

[5]: https://docs.gitlab.com/ee/operations/feature_flags.html
[^5]: [GitLab feature flags (archive)](https://web.archive.org/web/20241225100100/https://docs.gitlab.com/ee/operations/feature_flags.html)

[6]: https://developer.chrome.com/docs/web-platform/origin-trials
[^6]: [Chrome origin trials (archive)](https://web.archive.org/web/20241225173054/https://developer.chrome.com/docs/web-platform/origin-trials)

[7]: https://wiki.mozilla.org/Origin_Trials
[^7]: [Firefox origin trials (archive)](https://web.archive.org/web/20241207084320/https://wiki.mozilla.org/Origin_Trials)

[8]: https://snarky.ca/why-python-3-exists/
[^8]: [Why Python 3 Exists (archive)](https://web.archive.org/web/20241205000417/https://snarky.ca/why-python-3-exists/)

[9]: https://gcc.gnu.org/onlinedocs/gcc/Standards.html
[^9]: [GNU Compiler Collection standards (archive)](https://web.archive.org/web/20241227042615/https://gcc.gnu.org/onlinedocs/gcc/Standards.html)

