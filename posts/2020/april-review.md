<!--
.. title: Mad month of April
.. slug: april-review
.. date: 2020-05-18 12:21:00 UTC+10:00
.. tags: work, life, gitlab, squiz
.. category: 
.. link: 
.. description: 
.. type: text
-->

---

**EDIT: 2021-05-18** A year after I wrote this, I found it floating around on an old laptop which has been untouched. It's a bit of a time capsule, and interesting to compare with [what I later wrote](life-changes.html) about the same month. In some ways I think that it's better.

---

What a month April was! I've come through a turn of events and now things look stable enough that I can write about them.

TL;DR: Job change after COVID-19 fall-out, Staying at Home, loss and hope.

<!-- TEASER_END -->
----

## End of Time at Squiz

What was that I said in my last post about job security being a worry? Well...

After a long period at work, my family and I were looking forward to our bi-annual big trip. We had planned a four-week road-trip with our camping trailer, heading down the East coast of Australia from Melbourne, a few stops along coastal towns then a few nights in Sydney, make our way West to Bathurst and Orange, a brief stay at Lockhart and then back up South to Melbourne before sailing home.

The huge bush fires in December and January had caused us to pause, but then we decided to continue anyway: we were keen to spend in the affected areas!

And then the pandemic happened. Fortunately we had time to cancel/postpone our bookings. I shortened my leave from four weeks to two, and my leave began on the 6th of April.

At 2PM on the 6th of April, I received an SMS from my boss: urgent all-hands meeting and an email from the CEO of Squiz, could I read the email and then join the call in an hour?

The email contained a link to a Vimeo recording from our CEO, advising there were some difficult decisions about to be made, with a lot of staff to be made redundant, and others to be asked to take pay cuts, in a bid to keep the company viable. Oh, and please don't tell anyone until they are able to. We were told that everyone's position would be known and communicated within 48 hours. Way to start my vacation: I'm away for not even a day and the company starts to have trouble!

On Wednesday the 8th of April, I was told that unfortunately my position could no longer be filled, and my six-and-a-half years with Squiz would end on the 9th. After the meeting I confirmed that my VPN/LDAP access had been deactivated.

On the 9th of April we celebrated Owen's 8th birthday.

## GitLab

Prior to to all of this, on the 21st of March, I had been contacted out-of-the-blue by a "sourcer" at GitLab, asking if I was interested in discussing a [Support Engineer role](https://about.gitlab.com/job-families/engineering/support-engineer/) at GitLab. A few years ago the same happened for me with GitHub and I turned their recruiter down, kicking myself a bit later. I re-read the email, and the links to the RD and GitLab's [Handbook](https://about.gitlab.com/handbook/support/) and [Values](https://about.gitlab.com/handbook/values/). Then I thought a bit, and the next day was a bad one, so I decided "you know what? It can't hurt to talk to them" and I replied that I was interested in talking.

There was a questionnaire and then a screening call on Zoom (my first ever Zoom video call). That was on Friday the 3rd, early in the morning. It went well and they scheduled an interview for the 11th.

So, when I was laid off on the 8th/9th, the shock and disbelief were tempered by this thread of hope that I might be able to bounce to a new job, like I did from HP to Squiz back in 2013.

I had the first (technical) interview on the 13th, and it went very well. I felt like I'd aced it, answering bonus questions before they were even asked, and showing off a few Postgresql shell tricks to help me navigate the unfamilliar GitLab schema to find some data which was the exercise we were running.

I had a follow-up (behavioural) interview with the hiring manager (and future boss) on Tuesday 14th, and I was looking forward to it.

Overnight on the Monday, my Nan died.

## My Nan passed

Nan passed away in her sleep on the evening of Monday 13th, aged 96. Mother of five daughters including my mum, school headmistress and a faithful Anglican, it was bitter to be unable to attend her funeral in person (on the 15th) because of Physical Distancing requirements -- only ten people could attend, the rest of the familiy watched a live-streem from the funeral home on the Internet. A surreal, 21st-century experience to end a long 20th-century life of service to country and family. I have had to burry feelings to process "later", though she had been unwell for a few years. The last I saw her was in her bed at the home, echoes in my mind of my Pa in a similar condition years ago. Thankfully that was a good day and she was lucid, and recognised me: we hugged and kissed a fond farewell that day. I miss her, again.

## More GitLab interviewing

So, my mum called me in the morning about Nan, but I had to put that aside, because I had an interview at 11AM. It was another good interview, lots of [EQ questions](https://duckduckgo.com/?q=emotional+intelligence+interview+questions). It went well too, I was honest with the questions about "why do you want to join GitLab now?" and the accord I have with their Values. The feedback I got was that it was all looking positive

That night I was scheduled for *another interview*, this time with a Support Manager in Boston (Lee Matos, who it turns out is quite prominent in GitLab promotional videos...)

On the 15th (same day as Nan's funeral), I was contacted by recruitement staff at Squiz: they were organising support for recently laid off staff in finding work, including help writing CVs and coaching. I took them up on it, and had a great session with their recruiter on Friday before the interview with GitLab on Saturday morning.

That interview was a blast. Lee broke the ice with geeky subjects (Linux flavours, editors "I see you're an Emacs fan" - so he'd checked me out online ;-) ) before switching into the meat of the interview, where again, I felt comfortable just being myself and being honest. It went well and the closing feedback from Lee was that his report would be all positive, that I should relax and I'll know the outcome by the end of the following week.

## New job at GitLab

Well, it took a bit longer, but now I know the outcome: I've accepted an offer and signed a contract today to begin at GitLab on the 18th of May. I'm super excited about it. It feels like the Squiz job did at first. I am hoping that this time, as the technology is even more in line with my interests than Matrix was, that this will be a long-lasting passion.  The product's amazing, and the company literaly wrote the book on remote company operations. I don't know anything about Ruby or Rails (and GitLab knows this!) but what I've observed so far from installing *Omnibus* on my own machine and poking around in it, I like. A lot. Like, every time I read some code or look in the database, or scan log files, I'm smiling.  And the people are *my people* too. I'm also super excited to remain home: no more commuting to work. The "unlimited paid time off" is still something I'm trying to wrap my brain around, but it should mean that I can resume Code Club volunteering, or general Parent Help at school (once kids return to school, eventually, maybe next year?).
