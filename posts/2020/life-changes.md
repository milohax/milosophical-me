<!--
.. title: Life Change for 2020: new job, remote working
.. slug: life-changes
.. date: 2020-07-29 08:21:41 UTC+10:00
.. tags: work, work-life, life, gitlab, squiz
.. category: 
.. link: 
.. description: An update on the past few months, forced change, new beginnings, reflections on where I've been
.. type: text
-->

It's happened again: my old job is redundant due to circumstance beyond my control, and I was made to find a new one. Like last time, I found a new, better job very quickly. Unlike last time though, I was prepared, and had even started applying&hellip;

**2020-12-15: UPDATE** &mdash; this was written in July but unpublished. I have had a few people ask about how I got a new job so darned _fast_, and I promised to blog about it. I've finally moved the blog off from GitHub Pages, and now at the end of the year is as good a time as any to publish this.

<!-- TEASER_END -->

----

What a crazy time it's been! I had just started my first day of annual leave on April 6 &mdash; after postponing it from January, for the bush fires. We had planned a 4-week camping holiday along the East coast of Australia, and then COVID-19 happened, so we cancelled our plans altogether, and shortened the leave to two weeks, from home. Good thing that we did that, else we would have had to isolate in a camper-trailer at the side of a road somewhere! 

Anyway, first day of leave, and I got an SMS from my boss that there would be an "all hands" meeting at 2PM and there is an email from the CEO which I had better read/watch.  I read the email, which had a video link: JP with a long face, announcing Squiz company downsizing by about 1/5 [I learnt in July that the current downsize is more like 1/3] due to COVID-19 and the projected sales decline. Those impacted would know who they are within 48 hours, please keep it to yourself yadda, yadda.

Great, I can't even leave them for a day and this happens (kidding)!

I did a quick `git pull; git push` for all my work projects, in case it was me. Most of my tasks were already handed over or on hold because of my leave, so there was not a lot left hanging. Curious to reflect: the last significant task I'd done at Squiz was to install a TLS certificate for the Tasmanian Government's "coronavirus" web site.

On April 9 (O's birthday), I was told by video interview that my position is no longer required, effective immediately. I was reminded again not to announce this online until Squiz had the opportunity to inform their customers. So I couldn't even advertise that I was available for work at job sites.

They weren't kidding about the _immediately_ part! Right after the interview, all remote access was terminated &mdash; makes sense: I know too many passwords, including the admin password for everyone's MacBook. I'd be a significant security risk _if_ I was disgruntled. But, I couldn't even say goodbye to work colleagues on Slack or Zoom. I was contacted by phone within about half an hour by some of them: sysadmins who got notifications about my account being deactivated. Others who aren't even _with_ Squiz any more had found out through their own channels&hellip; you can't keep secrets from IT people!

After O's birthday, I spent that night in our detached studio, in the dark, murdering Van Halen tunes on my old Roland D-20 synthesizer. Mostly love ballads, weirdly. They wrote a _lot_ of those. Oh, and I figured out that the chord progressions on _Jump_ are actually different to what I had thought they were for years.

Yeah, I might have gone a little nuts.

Luckily for everyone: I'd already started applying for a new job.

## New job with GitLab

On March 21 I was approached by a "sourcer" (a talent scout, kind of) at GitLab, via my Linked In account:

> "Hi &hellip;  looking to hire a Support Engineer in APAC area. I see you have some interesting background in the field of Technical Support. Would you be interested in speaking to us? &hellip;"


This is not the first time I've been approached like this: it happened a few years ago from GitHub, too (before Microsoft bought them). At that time I was still in love with Squiz and replied "thanks, but no, thanks". I even told my boss about it once, I think he thought I was a bit strange. But I really did love working at Squiz. I have been kicking myself ever since: I should have at least talked to them. This time when Git*Lab* came knocking, I thought "_why the heck not?_" and so I replied that I was interested, and things went from there. This being GitLab, I don't have to describe the hiring process: [they have it online for anyone to read in their Handbook](https://web.archive.org/web/20200331194407/https://about.gitlab.com/handbook/hiring/interviewing/).

So, by the time Squiz announced on the 6th of April that I might be being laid off, I was past the screening interview on the 28th of March, and I already had a technical interview booked the following Tuesday, April 14.

On Monday night, 13th, I remember downloading GitLab and installing it in a VM (on Squiz's laptop - I had not returned it yet), poking around and learning where the logs are. It's my first Rails app, but it looks very orderly. Also it was so _easy_ to install, nothing like what I am used to.

That night my grandmother passed away, and my Mum called me about it on the Tuesday morning. I had to just _not think about that_, and focus on my next big tasks for the day.  So, after Mum's call, I put my grandmother in a mental box and went back to digging into GitLab's guts.

The technical interview went very well, I flew through the scenarios, even confessing that I don't know any Ruby or Rails, but showing off a few PostgreSQL and Linux skills I've picked up in Squiz, which helped me to sleuth around and answer the challenges I was set. I liked how I was coached through this, asked to think out loud and explore troubleshooting paths. I was invited for a behavioral interview (with my new boss) on the Wednesday. That interview was very positive, but I wouldn't let myself get too excited about it. I didn't want _more_ heartbreak.

I had a final interview with a Senior Support Manager (from Brooklyn) early (6AM) on the Saturday morning. This also went very well, we hit it off on a few topics. He told me that his opinion was I was a "strong yes" recommendation, and to relax:  I'll know within a few days.

### Landing the job offer
A really important decision which I made right at the beginning, when the GitLab sourcer contacted me, was to be authentic in this interviewing process. At the beginning that decision was easy, because I still had a job, but throughout the journey I stuck with it. I wanted to bring my whole self to my next job wherever it is, and so I didn't offer any pretence. I think that this helped me to come across as a genuine, transparent, approachable and likeable guy, someone who would be good for the team. I don't think I would have had the courage to do this without my years in Squiz.

It turned out to be a bit over a week before I was offered a position. It was a stressful time, but on reflection, not as long as it _seemed_ to be. I had opportunity to be with my wife and kids, to attend my grandmother's funeral _virtually_ (streamed on the Internet &mdash; she would have been very bemused), and try to get my head around what was going on in my life, and the world. Well, I didn't really manage either, but I was busy, anyway. The annual leave I had planned since November was _sort of_ happening; I needed to nest-build for my new life.

The paperwork and so on meant that I was made an official offer, with a contract to sign (electronically), and I started working for GitLab on the 18th of May.

Best call I've made in a long, long time.

---

## Leaving Squiz

Despite having GitLab in my back pocket, it was still a disappointment when I was let go on April 9th. I can't say that I was shocked very much: another reason for pursuing GitLab (apart from knowing that company and it's product are _awesome_) was that I already smelt this coming. For a few months now things at Squiz have seemed off, like the company wasn't quite operating properly. Sort of how things felt in my final months at HP. I told myself "_Don't worry about it, it's probably nothing, you don't know how to run a business anyway &mdash; this is why you're an employee, not an entrepreneur: so that you don't have to_". I had lost my enthusiasm though, and there were definitely aspects of the work and some of the people, that I found frustrating. Actually I think I was burnt out too.

After termination, Squiz offered to extend their employee wellbeing programme for 3 months, which was kind of them. Even more kind (actually really _awesome_) was that their hiring manager offered to ring around to find positions for people at other companies, and to coach for interviews. I took her up on that, and it was _super_ helpful. I do still have resources from when I was made redundant from HP, but I needed a refresher on interview technique, especially for those "emotional quotient" questions. You know the ones: "tell me something about yourself?", "what's a difficult situation you faced in the past and how did you handle it?", "how do you imagine your career progression at GitLab?", and so on. So we went over that and I had lots of time to think about how I really felt about those, and what my answers would be. It meant that I was not off-balance when I was asked because I was expecting it.

I made sure to call all my referees and the Squiz hiring manager to pass along the news that I'd accepted GitLab's offer.

Leaving Squiz is bitter-sweet. I really loved working there, looking forward to going to work each day, up until the last four or five months anyway. I learnt a lot about customer support, and grew in confidence when working directly with customers who are under duress. My technical skills went almost exactly in the direction I had hoped when I joined Squiz: I can now honestly say that I'm an experienced Unix/Linux sysadmin, that I'm comfortable with system automation tools and databases, as well as Internet/Web technologies. Those are wizard skills I've wanted since my days at EDS, especially when NetBank switched to Solaris in 2006 and we were building our own automations. While with Squiz I also got to travel interstate a few times, which was really great fun, especially visiting Perth. And I got to be a mentor, as well as to train new technicians! There are Squizzers across the world who I enjoyed working with and will miss a lot.

I think I most enjoyed my time in the office with co-workers. It was great to get to really know grumpy-bear Nathan "the velvet sledge-hammer", and the wise and calm Micky, our gurus who I learnt such a lot from. I especially loved working and growing with Dan and Darren, who started together with me on the same day in October 2013, and our boss Karl, who is the best boss I've ever had (even more than my NetBank boss, and she was awesome). Dan, Darren, Micky, and Karl are still working for Squiz (Nathan was laid off too, but has returned in a contractor position!), but the original "CRU" team has been broken up and they don't work together any more. Also since Squiz has effectively become "remote" they don't share an office. It's not the same, it's pretty sad actually. I wish them well, I will keep in touch with them, especially since they're here in Tasmania too.

![squiz angels pic](/img/Squiz-Angels.jpg)

I've been invited to the Hobart gang's Christmas Party on Thursday, can't wait to catch up!

---

## The GitLab Remote gig

Since I'm writing this during my "normal" working day now, I should tie it into an onboarding task I was given, which is to describe my early experience at GitLab.

To give you some idea of GitLab's _Transparency_ Value, here is GitLab's [position description page](https://about.gitlab.com/job-families/engineering/support-engineer/), and their [Handbook guide on responsibilities of the Support Engineer role](https://about.gitlab.com/handbook/support/support-engineer-responsibilities.html).

To the question of pay (since I know my uncle Tony will ask): GitLab pays local rates, and their assessment of the market meant that the best offer they could make for the position _is_ a 10% reduction in what I was previously being paid. I'm back to where I was when I started in Squiz, which itself was a about a 10% reduction from HP 7 years ago. I guess my pay grade is destined to be on sort of a 10% over 5 years sawtooth slope. **On the plus side**: I don't have to commute to an office, and I get free Internet. That makes up half the cash shortfall. We are comfortable this, with some small budget adjustments.

I'm in the top bracket for this position, which means I should be performing at the _Thriving in the Role_ level. Any pay increase will have to be along with a promotion.

GitLab are "all remote" and "asynchronous". They also make a big point of their Values, and [they actually live them](https://gitlab.com/mlockhart/lab/-/issues/25). I think it's a major part of how they can run a multinational company with no offices, just a post office box in downtown San Francisco, and all the timezones.

The conditions on this job are solid:

  * Unlimited Paid Time Off, up to 25 days _per vacation_, on top of any time for study or volunteering
  
  * Family First policy and the CEO encourages days off for family as well
  * All office equipment paid for. Free Internet, and when I'm on call, that month's phone bill, if I need to make international calls
  
I think the best is actually _no timesheets_! They don't track how many hours workers spend with bums in seats. _At my interview_, my _boss_ told me "I don't care if you play video games during the day, how you manage your time is up to you". Good-bye nineteenth century work values, I won't miss you.

Support roles typically don't enjoy _quite_ the freedom of other contributor roles, and this is the same at GitLab. But the roster is in no way onerous: 4 hours per day in normal hours, when you're at call, sometimes for one day of a weekend. Rotation is about 5 weeks and they are looking to expand it back out to 12 or 18. I do need to schedule time off around the roster, _or_ take responsibility for coverage in my absence. That's the best I've ever had anywhere.

GitLab are interested in Results. The company tracks its quarterly [OKR](https://about.gitlab.com/company/okrs/)s and [KPI](https://about.gitlab.com/handbook/ceo/kpis/)s. My performance is measured against my contribution to the [Support Team KPIs](https://about.gitlab.com/handbook/business-ops/data-team/kpi-index/#sts=Customer%20Support%20Department%20KPIs). So it is very clear _what_ I am expected to be doing and achieving, and how that is measured. How I achieve it is mainly up to me. That is liberating. I was already largely a "self-managed resource" at Squiz, now it's become something I'm fully responsible for.

---

So, an emotional roller-coaster between Squiz and GitLab. And that is just my work life. This year's also been pretty horrible globally, though I must say that personally, in our little corner of the world, we have escaped much of the trauma and only suffered through small lock-downs and life's natural upheavals. I and my family have been lucky in 2020. I'll close with one final observation: when Jenny was home-schooling the kids for lock-downs in the middle of the year, she insisted that they spend some outside time to decompress. The old trampoline which has been going largely unused for 5 years has never seen so much use. Even after the kids returned to school, they still spend every day on it, rain or shine. So that's something positive to have come out of all of this change.