<!--
.. title: Watching Apollo 13
.. slug: apollo13
.. date: 2021-06-18 12:21:00 UTC+10:00
.. tags: space, history, heros, inspiration
.. category: 
.. link: 
.. description: Some notes taken while watching the Apollo 13 movie "Houston we have a problem"
.. type: text
-->

Ron Howard's film _Apollo 13_ is a great telling of the adventure of NASA's third manned mission to land on the moon, in 1970. Being a _Hollywood_ movie, it of course makes changes in fact to highten drama and increase the story telling.

This is pointed out in [Universe Today](https://www.universetoday.com/)'s  [article](https://www.universetoday.com/101531/ken-mattingly-explains-how-the-apollo-13-movie-differed-from-real-life/) which summarises [NASA's November 6 2001 interview with Ken Mattingly](https://historycollection.jsc.nasa.gov/JSCHistoryPortal/history/oral_histories/MattinglyTK/MattinglyTK_11-6-01.htm).

My point in saying this is not to detract from the movie: it's a great movie, and I enjoyed watching it. I just feel that there is _so much more_ to explore in the mission, and to look at how things really were planned and thought out &mdash; even the lifeboat procedures and testing of the revival and re-entry had been thought about, not made up on the spot, though it was extensively revised.

Today I'm looking more closely into Mattingly's involvement in the mission. It's fascinating, and as a support engineer myself, I find it very inspiring.

<!-- TEASER_END -->

---

NASA has [a History article discussing Mattingly's removal from the Apollo 13 crew](https://www.nasa.gov/feature/50-years-ago-apollo-13-and-german-measles) which is well worth a read to put more context around Mattingly's involvement in the mission, beyond the movie's portrail as _the man who was passed over, and then tested procedures in the simulator_.

We are lucky to have detailed mission transcripts, as well as audio and recordings from the Apollo programme. As a public government department, NASA are required to keep all of this in the (American) public view, and they make it available on the Internet for the whole world.

An amazing archive site is [Apollo in Real Time](https://apolloinrealtime.org/). This gives a timeline for three of the Apollo missions: [Apollo 11](https://apolloinrealtime.org/11), [Apollo 13](https://apolloinrealtime.org/13), and [Apollo 17](https://apolloinrealtime.org/17). You can scrub through the entire mission timelines, or jump to specific milestones, viewing audio/video from the archives of all communications channels, and photographs, all sync'd together with a transcript, commentry and milestone overview. There's even [a milestone for when Ken Mattingly enters the control room](https://apolloinrealtime.org/13/?t=-00:53:20) after the launch.

In fact with AiRT, you may search for things. Continuing with my focus on Mattingly, I found that [he did actually read procedures](https://apolloinrealtime.org/13/?t=126:14:12) for reviving _Odyssey_ to the Apollo 13 crew (Jack Swigart). Just listening to his calm voice, and how he fields questions with his peer experts is amazing to listen to.

With all this official recording, their talk about the water condensation in the CM, and the camaraderie displayed, it brings new depth to the story, even better than the movie. I'm having a great time pouring over all of it.

While it makes use of NASA archive material, AiRT itself is not a NASA production. It is produced by [Ben Feist](https://twitter.com/benfeist) and many volunteer researchers and web developers. It's an outstanding resource for watching and researching these Apollo missions, and worth your time exploring. Do check out the Instructions/Credits section.
