<!--
.. title: Python Environments (again!)
.. slug: python-virtualenv-again
.. date: 2021-09-29 07:51:49 UTC+10:00
.. tags: python, tip, bash, fish
.. category: 
.. link: 
.. description: Revisiting how to run multiple Python environments.
.. type: text
-->

Here I am again, trying to run multiple Pythons, with multiple libraries, without them tripping over each other. Many hackers have gone down this path and become lost, I expect I will fare no better, but I've stumbled upon a scheme which works for me, whether running Python on a Macintosh, in a Linux server, or _even on Windows_.

<!-- TEASER_END -->

---

Like everyone, I have been [having a love-hate relationship with python virtual environments](/tags/python.html). I even had [a set of bash functions to wrap virtualenv, like virtualenvwrapper](/blog/2019/python-virtualenv-wrappers.html). This _worked_, but had some problems, and while it supported Conda and Virtualenv, it didn't address _managing_ multiple different Python _runtimes_.

## Which Python manager to use, and when?

What I think I have decided today is this:

1. Avoid using &ldquo;system&rdquo; pythons for programming projects, including Homebrew on macOS &mdash; that counts as a system python.
    * System pythons are for use by utilities and applications which are installed and managed for you by the OS package system.
    * They can _and do_ change unexpectedly (particularly Homebrew is bad at this), which breaks dependent virtual environments, making them _unsuitable_ for doing programming work
    * Leave system python runtimes to your package system and develop with your _own_ python runtimes.
1. Use `conda` ([Anaconda](https://anaconda.org/)) for setting up anything &ldquo;complicated&rdquo; (Scientific stuff, CUDA, Jupyter, [using pygame on a Mac](https://gitlab.com/milohax-hub/Games-with-Pygame/-/issues/1), or using python _at all_ on Windows).
    * Conda supports using multiple environments, and _it just works_.
    * You _can_ use `pip` in a `conda` environment, for PyPI packages which aren't provided by Anaconda, or Conda Forge.
    * *_But_* for other development projects (or Linux server deployments) it may be easier to use &ldquo;traditional&rdquo; light-weight python environments<br/>
    ([miniconda](https://docs.conda.io/en/latest/miniconda.html) is lightweight, but more difficult to set up than Anaconda Full, at least in Mac/Windows, and it is less common in servers).
1. Use [ASDF](https://github.com/asdf-vm/asdf) to manage [multiple pythons](https://github.com/danhper/asdf-python) (but not venvs) for development and testing.
    * This deserves its own blog post, I have a [few issues about this](https://gitlab.com/milohax-net/radix/bootstrap/-/issues?scope=all&state=all&search=python) in another project.
    * ASDF uses [pyenv](https://github.com/pyenv/pyenv) under the hood. Pyenv compiles python runtimes, Unix Ports-style.
    * *_But_* some versions of python [can't be compiled on newer macOS](https://github.com/pyenv/pyenv/issues?q=is%3Aissue++build+failed++macos) (e.g. Python 3.6).
1. As a last resort (or _first_ option on a server), use [Docker Containers](https://www.docker.com/resources/what-container), or [virtual machines](https://www.virtualbox.org/), to host python environments.
    * These run python on the Linux kernel in the container/virtual machine.
    * You can install _any_ version of python, typically as part of the [container image](https://www.docker.com/products/docker-hub), or as a package of the [guest OS](https://www.virtualbox.org/wiki/Guest_OSes).
    * It's how I run this blog's site generator, since that needs python 3.6.
    * Manage multiple pythons _at the machine/container level_, one single environment per machine, which side-steps the multiple pythons issue entirely.
    * *_But_* these can be more difficult to manage, and require virtualization hardware and software.
    * *_And_* access to the host environment (e.g. to interface with files or games running on the host) is more difficult.

## Virtual environments

1. In unix workstations, [Pipx](https://pypa.github.io/pipx/) is a nice way to install PyPI _applications_ &ldquo;stand-alone&rdquo; &mdash; for instance `bpytop`, or `ansible`. These dedicated environments are lightweight and linked the python which runs `pipx`, by default (this can be overridden with the `--python` option).
    * Pipx is installed as a Homebrew package on macOS, and a pip package elsewhere (see my environment variable settings below, first): `brew install pipx`, or `python -m pip install --user pipx`
1. For python virtual environments, if you can't use Conda environments (because you are not using Conda), then use [virtualenv](https://virtualenv.pypa.io/en/latest/index.html) &mdash; it's the next most mature option.
    * I've [flip-flopped on this decision](https://gitlab.com/milohax-net/radix/bootstrap/-/issues/3) a few times in the past, but I have [come back into the virtualenv fold](https://gitlab.com/milohax-net/radix/bootstrap/-/issues/3) for vanilla python, and also using conda to work around virtualenv's limits for Windows and Mac.
    * You can install virtualenv with pipx: `pipx install --python $(asdf which python) virtualenv`
        * If you installed pipx with Homebrew, then this means that the _default_ python for any virtualenv-managed environment is _also_ Homebrew's python.
        * Since this is changeable on Homebrew's whim, you need to override it with `--python $(asdf which python)` to use the ASDF global python.
1. Python's built-in [venv module](https://docs.python.org/3/library/venv.html) is the [PEP 405](https://www.python.org/dev/peps/pep-0405) standard, but it's less capable than _virtualenv_ and slower.
1. I've always disliked [virtualenvwrapper](https://pypi.org/project/virtualenvwrapper/), but [VirtualFish](https://github.com/justinmayer/virtualfish) (despite the unhelpful name) provides the same functionality, with better names, and Tide prompt support.
    * Install virtualfish: `pipx install --python $(asdf which python) virtualfish`, (to avoid Homebrew as the default python for venvs)
    * Then install it into fish with `vf install; alias vp=vf; funcsave vp`.
1. [Avoid using pipenv](https://chriswarrick.com/blog/2018/07/17/pipenv-promises-a-lot-delivers-very-little/). It only offers dependency locking and some convenience (e.g. &ldquo;automatic&rdquo; activation via pipenv shell), at the cost of brokenness. These are better provided by other means:
    * [pip-tools](https://pypi.org/project/pip-tools/) pinned/locked pip dependencies with the ability to keep them fresh
    * VirtualFish has a plugin for automatic environment activation, or else [use direnv](https://direnv.net/), [like this](https://github.com/direnv/direnv/wiki/Python).


### Configuring default locations for venvs

I have [strong opinions](/blog/2016/home-dir-maintenance.html) on where things belong. Here are some unix environment variables to control where virtualenvs or pipx applications are installed:

```sh
#virtualenv / virtualfish base directory
#https://pipenv.pypa.io/en/latest/advanced/#custom-virtual-environment-location
WORKON_HOME=~/lib/venv
#https://github.com/justinmayer/virtualfish/blob/main/virtualfish/virtual.fish
VIRTUALFISH_HOME=~/lib/venv

#PIPX https://pypa.github.io/pipx/docs/
PIPX_HOME=~/lib/pipx
PIPX_BIN_DIR=~/bin
```
