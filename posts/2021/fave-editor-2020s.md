<!--
.. title: My Favorite Text Editor in the 2020s
.. slug: fave-editor-2020s
.. date: 2021-11-08 20:09:36 UTC+11:00
.. tags: editor, emacs, vim, netbeans, jEdit, vs-code
.. category: 
.. link: 
.. description: An explanation for "what's your favorite text editor, and why? I explore the similarities of VS Code and my previous favorite EMACS, and illustrate that Code is the new EMACS for the 21st century."
.. type: text
-->

For decades, my favorite place to write has been in the venerable text editor / programmer's environment [EMACS](/tags/emacs.html). It's a superb [LISP](/tags/lisp.html) environment, with a long history, is [fantastically hackable](/blog/2016/spacemacs-01.html), and has a mode for nearly everything.

But [for the past three years](https://gitlab.com/milohax/dotspacemacs/-/commits/master), I've abandoned it for a new friend, from a most unlikely source: Microsoft's _Visual Studio Code_. You know, [the _Flight Simulator_ company](https://en.wikipedia.org/wiki/Microsoft_Flight_Simulator) `;-)`

This is a big change for me. Why after all of this time have I done this? The simple answer is: _it Just Works_. It's EMACS' spiritual successor in many ways.

<!-- TEASER_END -->

---

## What's so great about Visual Studio Code?

We computer hackers get attached to our tools, much like carpenters. It takes some convincing to make one check out a new tool, when the old faithful is so comfortable in the hand. What made me pick up Visual Studio Code in the first place, when I'm so happy with EMACS?  

[Visual Studio Code](https://code.visualstudio.com/) takes the best ideas of EMACS, and puts them into a modern text editor:

* Open Source and freely shareable and hackable
* Cross-platform, available for Windows, Macintosh, and Linux
* Automatic settings synchronization across different computers
* A fast core, with an extension system based on the Web technology stack, that's widely understood
* A free and open extensions bazaar
* It's interface takes _first class_ advantage of graphical displays
* Supports version control with `git`, right out of the package
* A _command pallette_ to search for files, or to enter commands not bound to a key, which is like EMACS' _minibuffer_
* Simple configuration system with a graphical front-end, but with access to a text-based (JSON) configuration file (with the **Preferences: Open Settings (JSON)** command)
* Deep code introspection and debugging abilities
* Access remote filesystems and servers
* A built-in terminal emulator

The name _Visual Studio Code_ is a mouthful, and also the _Visual Studio_ part is pretty much just a marketing stance: it's not very much like Microsoft Visual Studio really, any more than _Sun Java Desktop System_ was like Java. It's more like Sublime Text, or Atom, whereas Visual Studio is a full IDE, like JetBrains, or NetBeans. So let's just call it _Code_, or _VS Code_.

## It's just so gorgeous to look at

![screen shot of VSCode](/img/code-editor.png)

Here we see that I've put the Sidebar on the right, and the Minimap on the left (à la Athena/Smalltalk scrollbars). The icons are customized with the [vscode-icons](https://marketplace.visualstudio.com/items?itemName=vscode-icons-team.vscode-icons) and [file-icons](https://marketplace.visualstudio.com/items?itemName=file-icons.file-icons) extensions. The colour theme is [Kimbie Dark+](https://marketplace.visualstudio.com/items?itemName=dnamsons.kimbie-dark-plus) (to match my blog's colours), but I often switch them up. I'm using it to edit this blog post. You'll notice a [couple](https://marketplace.visualstudio.com/items?itemName=jyee721.gitlab-snippets) of [GitLab extensions](https://marketplace.visualstudio.com/items?itemName=GitLab.gitlab-workflow), and the [Docker extension](https://marketplace.visualstudio.com/items?itemName=ms-azuretools.vscode-docker) makes it easy to launch Nikola to build and review the post.

### Code is _Fast_

You can start up Code, fully configured and ready to edit, in just a few seconds, even on modest hardware. If Code is already running then using the `code` command from a shell will cause it to just open a new window (like `emacsclient`).

The terminal emulator has an optimised renderer which will use your GPU and can spit out thousands of lines per second. Many extensions require a "reload" (akin to a page refresh on a web browser), but this is also very quick.

That Code is so zippy was a surprise for me. Code is based upon the [Electron](https://github.com/electron/electron) application framework. Prior to Code, my experience in using a text editor based on Electron was the [Atom text editor](https://atom.io/) (a hackable editor for the twenty-first century). Atom showed promise, but was painfully slow to use. Somehow Microsoft have solved this issue in Code, and it can handle very large files, intelligently disabling some features like language services, or syntax highlighting when files get above a certain size.

### Code is _Easy to Customize_

The **Preferences: Open Keyboard Shortcuts** command (`C-K C-S`) gives a _searchable_ list of commands, their current key bindings, when they are in effect (which mode), and where the binding comes from (a default setting, part of an extension, or a custom setting). So this is essentially like EMACS' `describe-key` (`C-h k ...`).

The Settings interface is pretty much exactly like EMACS' interactive menus, too. I like that Code has global settings, and then workspace-specific overrides. You can also view the settings file and edit it directly, or show only the settings you have modified.

### There are _Extensions for everything_

There is nothing Code does, which EMACS cannot do too, but Code does it without one having to do a lot of research, and without making complex configuration changes in code to install modules: just a few mouse clicks and you can add Python language support with full virtual environment support, Intellisense, and interactive debugging. Want to do it remotely? No dramas, there is an extension which provides that, too.

### Code is _Easy to Use_

* The Command Pallette (`C-S-p`), and File Search (`C-p`) are like EMACS' [which-key](https://github.com/justbur/emacs-which-key) (installed in Spacemacs, but you have to add it to EMACS otherwise)
* `Ctrl`/`Command` keys are used as expected, not clashing with the 1970s conventions of EMACS/ITS. On Windows and Linux, you use the `Ctrl` key for most bindings, and on Macintosh you instead use the `Command` key
* Snippets are already set up and ready to go with some supplied examples

## Where is EMACS lacking?

Code is where EMACS has been headed for a long time, but was unable to pass through Zeno's *dichotomy* paradox to reach mainstream levels of usability. Why should this matter to me, after all I've spent years tweaking it to my liking? Well it's because even after all this work, EMACS is still operated like a 1980s LISP Machine, and not like the rest of my computers. As awesome as that environment is, it does mean that I must switch my own "operating mode" when I pass between EMACS, and a web browser. The options to overcome this are:

1. Make EMACS behave like a modern (okay, late 1980s), [CUA interface](https://en.wikipedia.org/wiki/IBM_Common_User_Access) with [sensible copy/paste shortcuts](https://en.wikipedia.org/wiki/Cut,_copy,_and_paste#Common_keyboard_shortcuts). Turns out this is quite difficult
1. Make the browser behave like EMACS, which is possible with some browser extensions, but incomplete and clunky. I never liked EMACS' `C-n`, `C-p`, `C-f`, and `C-b` keys, preferring cursor keys always, so other applications which have an "EMACS mode" that only offers this, are no use for me
1. Use EMACS as a web browser. Yes, [it can do it](https://www.emacswiki.org/emacs/CategoryWebBrowser), no I wish it wouldn't

Many things are possible in EMACS, in many different ways, but there is not an agreed way. I understand the philosophy of this, but it does make it harder to learn how to use EMACS to (for instance): [manage Vagrant](https://develop.spacemacs.org/layers/+tools/vagrant/README.html), or [Docker](https://develop.spacemacs.org/layers/+tools/docker/README.html), or to [build Java code](https://www.emacswiki.org/emacs/JavaDevelopmentEnvironment), or to access things remotely the right way.

Be careful customizing EMACS: frequently when adding things to the configuration, other things break.

Also if you want to see where else EMACS is lacking, try running it on a non-GNU system. It relies upon many of the tools from GNU  `core-utils`. I switched to Apple Macintosh in 2016 (a topic for another blog post!) and while you can make it work on macOS: it's slower, and less stable. It was getting to the point that I didn't trust EMACS not to crash on me in the middle of a remote edit. It's not really practical to run EMACS in Windows except in WSL2. Back in the mid-2000s I tried using Cygwin, but that was just torturous, so I had a stint in [jEdit](/tags/jedit.html) instead.

EMACS is 1980s technology. This is not a problem usually, but:

* The UI is clunky on a modern machine
* It is single threaded, not thread safe
* The extension language is primitive compared to full/modern LISP, and many libraries rely on a global namespace, so this is hard to change.
* A [fully customised Spacemacs configuration](https://gitlab.com/milohax/dotspacemacs), while awesome, is slow to start, requiring 10-20 seconds on modern computers
* Those key bindings... _sigh_
* Some of the terms are dated, or have newer meanings (_point_, _window_, _frame_, _kill ring_, _meta-key_). It's quaint, which is nice for us old farts, but it is a barrier for newbies

There have been efforts to change these problems, updating EMACS to run on Guile and use the GTK for a widget set. After 20 years we still aren't there yet with Guile instead of eLisp, and GTK widgets are fine but not pretty (at least not in EMACS).

### Some EMACS modes are still irreplaceable

I keep EMACS around for these tools, though I'm using them less these days.

* [Org Mode](https://orgmode.org/), while emulated, does not have a feature-complete replacement outside of EMACS
* [Magit](https://magit.vc/) in many ways is a superior way to work with git repositories than even the best Git extensions for Code or other interfaces
* I like [eshell](https://masteringemacs.org/article/complete-guide-mastering-eshell) (although [fish](/tags/fish.html) is nice too!), and [dired](https://masteringemacs.org/article/dired-shell-commands-find-xargs-replacement)

## The popular kid on the block

There is a lot to be said for using an editor which is popular. It means that there's a healthy improvement cycle, a big extensions bazaar, and a lot of tips online for making the most out of it. Code is easily the most popular editor environment today:

* [Most Popular Development Environments](https://insights.stackoverflow.com/survey/2019#development-environments-and-tools) (Stack Overflow, 2019, Code is more popular than EMACS, Vim, and Netbeans combined)
* [Most popular technologies](https://insights.stackoverflow.com/survey/2020#most-popular-technologies)  (Stack Overflow, 2020, editors not explored, but look at Javascript)
* [Most popular Integrated Development Environment](https://insights.stackoverflow.com/survey/2021#section-most-popular-technologies-integrated-development-environment) (Stack Overflow 2021)

## Hackability?

Does this matter to me? Much of my time was spent (joyfully, but wastefully) hacking EMACS Lisp codes because it's [so easy to explore online](/blog/2018/4-bits-part3.html). 

The keyboard is much easier to customize, and is already "modern" right out of the package. This has always been the main thing that I wanted to "fix" with EMACS, ever since the 1990s when I started to use it. I got some ways towards it, but always I've been fighting it.

Exploring "how things work" is harder in Code, since the Electron environment is not as dynamic as eLisp. But it's still _possible_. It doesn't matter to me as much though &mdash; I just need to know what happens when I press a key, or to bind a key to a function, which Code can tell me. If ever there is a need for a new extension, I'll learn the programming API by following [the tutorial](https://code.visualstudio.com/api/get-started/your-first-extension).

Super sweet, super easy, Code is a comfortable place to be.
