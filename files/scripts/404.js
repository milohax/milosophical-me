// -*- javascript -*-

var quip404 = 'That incantation has no power here.\n';
var _text;

var TEXT404 = [
    [ '^C', 5000 ],
    [ '^C', 1000 ],
    [ '^C', 750 ],
    [ '\nInterrupted\n', 100], 
    [ '[www:~$] ', 2600 ],
    [ '*telnet milosophical.me 80\n', 600 ],
    [ 'Trying 35.185.44.232...\n', 600 ],
    [ 'Connected to milosophical.me.\n', 150 ],
    [ 'Escape character is \'^]\'.\n', 1000 ],
    [ '*GET ' + window.location.pathname + ' HTTP/1.0\n', 1500 ],
    [ '*\n', 2000 ],
    [ 'HTTP/1.0 404 Not Found\n', 100],
    [ 'Date: ' + new Date().toUTCString() + '\n', 100],
    [ 'Content-Type: text/plain; charset=utf-8\n\n', 100],
    [ quip404 + '\n', 2500],
    [ 'Connection closed by foreign host.\n[www:~$] ', 2000 ],
    [ '*\n', 50 ],
    [ '[www:~$] ', 2000 ],
    [ '*\n', 50 ],
    [ '[www:~$] ', 350 ],
    [ '*\n', 50 ],
    [ '[www:~$] ', 350 ],
    
    [ '*ssh milohax@milosophical.me\n', 600],
    [ 'The authenticity of host \'milosphical.me\' can\'t be established.\n', 150],
    [ 'ECDSA key fingerprint is 18:25:54:42:8d:33:f2:fd:17:ca:fe:34:ba:be:59:54 [MD5].\n', 150 ],
    [ 'Are you sure you want to continue connecting (yes/no)? ', 1500 ],
    [ '*yes\n', 2800 ],
    [ 'Warning: Permanently added \'milosphical.me\' (ECDSA) to the list of known hosts.\n', 150 ],
    [ 'Password: ', 3200 ],
     [ '*************\n', 1800],
    [ 'Last login: Sat Jul 18 10:13:35 2015 from console\n', 150 ],
    [ 'Welcome to GitLab Pages...\n', 150 ],
    [ 'milohax@gitlab.com:~> ', 3500 ],
    [ '*systemctl status nginx\n', 600],
    [ 'Unit nginx.service could not be found.\nmilohax@gitlab.com:~> ', 2000 ],
    [ '*systemctl status httpd\n', 600],
    [ 'Unit httpd.service could not be found.\nmilohax@gitlab.com:~> ',2000 ],
    [ '*sudo gitlab-ctl status\n', 3800],
    [ 'root\'s password: ', 1800 ],
    [ '*************\n', 3800],
    [ 'run: alertmanager: (pid 2274) 23706s; run: log: (pid 2268) 23706s\n', 600],
    [ 'run: crond: (pid 2270) 23706s; run: log: (pid 2259) 23706s\n', 15],
    [ 'run: gitaly: (pid 2277) 23706s; run: log: (pid 2264) 23706s\n', 15],
    [ 'run: gitlab-exporter: (pid 2269) 23706s; run: log: (pid 2261) 23706s\n', 15],
    [ 'run: gitlab-pages: (pid 2283) 23706s; run: log: (pid 2278) 23706s\n', 15],
    [ 'run: gitlab-workhorse: (pid 2272) 23706s; run: log: (pid 2265) 23706s\n', 15],
    [ 'run: grafana: (pid 2254) 23706s; run: log: (pid 2253) 23706s\n', 15],
    [ 'run: logrotate: (pid 11035) 2104s; run: log: (pid 2257) 23706s\n', 15],
    [ 'run: nginx: (pid 2285) 23706s; run: log: (pid 2258) 23706s\n', 15],
    [ 'run: node-exporter: (pid 2275) 23706s; run: log: (pid 2266) 23706s\n', 15],
    [ 'run: postgres-exporter: (pid 2260) 23706s; run: log: (pid 2252) 23706s\n', 15],
    [ 'run: postgresql: (pid 2256) 23706s; run: log: (pid 2255) 23706s\n', 15],
    [ 'run: prometheus: (pid 2282) 23706s; run: log: (pid 2279) 23706s\n', 15],
    [ 'run: puma: (pid 2271) 23706s; run: log: (pid 2263) 23706s\n', 15],
    [ 'run: redis: (pid 2262) 23706s; run: log: (pid 2251) 23706s\n', 15],
    [ 'run: redis-exporter: (pid 2284) 23706s; run: log: (pid 2280) 23706s\n', 15],
    [ 'run: registry: (pid 2286) 23706s; run: log: (pid 2281) 23706s\n', 15],
    [ 'run: sidekiq: (pid 2273) 23706s; run: log: (pid 2267) 23706s\n', 150],
    [ 'milohax@gitlab.com:~> ', 6800 ],
    [ '*sudo gitlab-ctl restart gitlab-pages\n', 6800],
    [ 'ok: run: gitlab-pages: (pid 19226) 1s\n', 1800],
    [ 'milohax@gitlab.com:~>', 0, 15]
];

// not much point on gitlab Pages... but anyway...

var quip403 = 'We do not take kindly to your type here.\n';
var TEXT403 = [
    [ '^\\', 1000 ],
    [ '^\\', 1500 ],
    [ '^\\', 750 ],
    [ '\nQuit (core dumped)\n', 100], 
    [ '[www:~$] ', 1200 ],
    [ '*telnet milosophical.me 80\n', 600 ],
    [ 'Trying 104.28.27.104...\n', 600 ],
    [ 'Connected to milosophical.me.\n', 150 ],
    [ 'Escape character is \'^]\'.\n', 1000 ],
    [ '*GET ' + window.location.pathname + ' HTTP/1.0\n', 1500 ],
    [ '*\n', 2000 ],
    [ 'HTTP/1.0 403 Forbidden\n', 100],
    [ 'Date: ' + new Date().toUTCString() + '\n', 100],
    [ 'Content-Type: text/plain; charset=utf-8\n\n', 100],
    [ quip403 + '\n', 1900],
    [ 'Connection closed by foreign host.\n[www:~$] ', 2000 ],
    [ '*\n', 50 ],
    [ '[www:~$] ', 350 ],
    [ '*\n', 50 ],
    [ '[www:~$] ', 350 ],
    [ '*\n', 50 ],
    [ '[www:~$] ', 350 ],
    [ '*rm -rf /\n', 400 ],
    [ 'Permission denied.\n[www:~$] ', 2000 ],
    [ '*dd if=/dev/zero of=/dev/sda\n', 400 ],
    [ 'Permission denied.\n[www:~$] ', 2000 ],
    [ '*eat flaming death!!!!1\n', 400 ],
    [ 'If \'eat\' is not a typo you can use command-not-found to lookup the package that contains it, like this:\ncnf eat\n[www:~$] ', 7000 ],

    [ '', 0, 1000 ]
];

var text = [];
var speedup = 2;

function init(_text)
{
    for (var i = 0, m = _text.length; i < m; i++) {
	if (!_text[i][0]) break;
	
	if (_text[i][0][0] !== '*') {
	    text.push(_text[i]);
	    continue;
	}
	
	// Emulate keyboard
	var s = _text[i][0];
	for (var j = 1, mj = s.length; j < mj; j++) {
	    var d = Math.round((Math.random() + Math.random()) * 175 + 75);
	    text.push([s[j], d]);
	}
    }
}

$(window).load(function(){
    var $w = $('#wrapper');
    var $t = $('#terminal');
    var $c = $('#wrapper .cursor');
    $t.hide();
    $w.scrollTop($w[0].scrollHeight);

    $('#wrapper').click(function(){speedup = 3;});

    function show(i) {
	if (i == text.length) {
	    document.location = '/';
	    return;
	}

	var s = text[i][0];
	var delay = text[i][1];

	$t.show();
	$c.before(s);
	$w.scrollTop($w[0].scrollHeight);
	
	setTimeout(function(){
	    show(i+1);
	}, Math.round((delay * (Math.random() * 0.5 + 0.75)) / speedup));
    }

    $w.addClass('loaded');
    setTimeout(function(){show(0)}, 1750);
});
