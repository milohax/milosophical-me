This is a mirror of the Jargon File.

You can see the original at http://www.jargon.org  or http://catb.org/jargon.

This copy was cloned in 2016.

In 2018 the glossary was [transcoded from ISO-8859-1 to utf-8](https://milosophical.me/blog/2018/latin1-to-utf8.html).
