FROM registry.gitlab.com/milohax/milosophical-me

# Install custom tools, runtimes, etc.
# For example "bastet", a command-line tetris clone:
# RUN brew install bastet
#
# More information: https://www.gitpod.io/docs/config-docker/

#CMD [ "/usr/bin/nikola auto" ]
