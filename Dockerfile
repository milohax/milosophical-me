FROM registry.gitlab.com/paddy-hack/nikola:7.8.15
LABEL  Mike Lockhart <sinewalker@gmail.com>

RUN apk add --no-cache curl unzip file
