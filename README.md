[![Gitpod ready-to-code](https://img.shields.io/badge/Gitpod-ready--to--code-blue?logo=gitpod)](https://gitpod.io/#https://gitlab.com/milohax/milosophical-me)

## Milosophical Me - src branch

This is my personal web site and blog. In this `src` branch (which is the default branch) are the source input files that make up the site, as well as scripts and configuration to build it.

The site is generated using the [Nikola](https://getnikola.com) static blog generator. The site is deployed to GitLab pages by GitLab CI/CD, which runs Nikola in a Docker Runner on GitLab.com and uses the output which Nikola generates to the `public/` directory.  100% of the content of `public/` comes from this `src` branch, including the mirror of [the jargon file](http://www.catb.org/jargon/).

To speed up the build, the CI/CD uses the cached renderings and Do It database from the last Pages deployment. This is a bit of a space waste, but that's the classic "time vs storage" performance trade-off. The files aren't browseable from http://milosophical.me, anyway.

Refer to [this project's wiki](https://gitlab.com/milohax/milosophical-me/-/wikis/home) for details about how the site works and how to maintain it.  I also use the [Issues](https://gitlab.com/milohax/milosophical-me/-/issues) to keep track of what needs special attention.

## Hacking options

 * Clone and edit/test/publish locally, by either:
    * Use the `dsh` or `dnikola` scripts to run Nikola in a Docker container on your workstation (requires Docker Desktop)
    * Run Nikola in a Python virtual environment (requires Python 3.6, not newer, and the packages from `requirements.txt`)
 * or [use Gitpod](https://gitpod.io/#https://gitlab.com/milohax/milosophical-me/) to develop in your browser, with [VS Code](https://code.visualstudio.com/) and Nikola running in [auto mode](https://getnikola.com/features/index.html#auto).

Merge/push changes to `src` to [have GitLab build and deploy](https://getnikola.com/handbook.html#automated-rebuilds-github-actions-gitlab) with [CI/CD](https://gitlab.com/milohax/milosophical-me/-/blob/src/.gitlab-ci.yml).
