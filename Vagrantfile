# -*- mode: ruby -*-
# vi: set ft=ruby :

# All Vagrant configuration is done below. The "2" in Vagrant.configure
# configures the configuration version (we support older styles for
# backwards compatibility). Please don't change it unless you know what
# you're doing.
Vagrant.configure("2") do |config|
  # The most common configuration options are documented and commented below.
  # For a complete reference, please see the online documentation at
  # https://docs.vagrantup.com.

  # Set the Vagrant name (replaces 'default')
  config.vm.define "milosophical.me"
  
  # I like openSUSE more than Ubuntu, and Leap 15.0 has the right version of
  # Python (3.6) to suite the older Nikola 7.8 that milosophical.me uses.
  config.vm.box = "bento/opensuse-leap-15.0"

  # Don't auto-update the Virtualbox Guest Editions - older opensuse-leap box
  # is only compatible with the pre-bundled editions.
  config.vbguest.auto_update = false
  
  # Disable automatic box update checking. If you disable this, then
  # boxes will only be checked for updates when the user runs
  # `vagrant box outdated`. This is not recommended.
  # I'm deliberately using an outdated box to go with outdated app software.
  config.vm.box_check_update = false

  # Create a forwarded port mapping which allows access to a specific port
  # within the machine from a port on the host machine. In the example below,
  # accessing "localhost:8080" will access port 80 on the guest machine.
  # NOTE: This will enable public access to the opened port
  config.vm.network :forwarded_port, guest: 8000, host: 8000

  # Provider-specific configuration so you can fine-tune various
  # backing providers for Vagrant. These expose provider-specific options.
  # Example for VirtualBox:
  #
  config.vm.provider "virtualbox" do |vb|
  #   # Display the VirtualBox GUI when booting the machine
  #   vb.gui = true
  #
  #   # Customize the amount of memory on the VM:
      vb.memory = "2048"
      vb.cpus = 2
      vb.name = "milosophical.me"
  end
  #
  # View the documentation for the provider you are using for more
  # information on available options.

  # Enable provisioning with a shell script. Additional provisioners such as
  # Ansible, Chef, Docker, Puppet and Salt are also available. Please see the
  # documentation for more information about their specific syntax and use.
  config.vm.provision "shell", inline: <<-SHELL
    zypper install -y git htop mc command-not-found psmisc\
      lbzip2 libopenssl-devel readline-devel \
      libxml2-devel  libxslt-devel  zlib-devel
    zypper install -y python3

    python3 -m venv /home/vagrant/nikola
    source /home/vagrant/nikola/bin/activate
    pip install --upgrade pip
    ln -s /vagrant /home/vagrant/milosophical.me
    pip install -r /vagrant/requirements.txt
    chown -R vagrant:vagrant /home/vagrant/nikola
    vbashrc=/home/vagrant/.bashrc
    echo 'source ~/nikola/bin/activate' >> ${vbashrc}
    echo 'cd ~/milosophical.me' >> ${vbashrc}
  SHELL
end
