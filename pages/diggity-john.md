<!--
.. title: Diggity John Who Lived in a Billabong!
.. slug: diggity-john
.. date: 2021-05-30 10:53:45 UTC+10:00
.. tags: kids, writing, poetry
.. category: family
.. link: 
.. description: A short poem about finding friends despite prejudice.
.. type: text
.. author: Ryan
-->

&mdash; by Ryan, aged 10.

Diggity John, he lived in a billabong, lonely be he.<br/>
Diggity John, who lived in a billabong, went for a walk, <br/>on a day as good as December may be.

Diggity John, who lived in a billabong, went up to Farmer Gong.<br/>
"Will you be my friend?" asked Diggity John, who lived in a billabong.<br/>
"No!" Said Farmer Gong. "Diggity John who lives in a billabong, be off with ye!<br/> Humans and bunyips can't be friends, that's how it simply be!"

So Diggity John who lived in a billabong, off went he, <br/>to find  another friend.

Diggity John, who lived in a billabong, he met a kangaroo. <br/>
"SHOO! Diggity John who lives in a billabong,SHOO! Be off with you!"<br/>
And with a bing and a bong, <br/> old kangaroo went hopping along, <br/>poor old Diggity John, who lived in a billabong, <br/> didn't even get to ask.

Diggity John, who lived in a billabong, went walking along, <br/>to find a Possum, <br/>watching the flowers blossom.<br/>
"Will you be my friend?" asked Diggity John, who lived in a billabong.<br/>
But the possum, he, <br/>just scrambled up a tree, <br/>shouting:
"I'm alive, HURRAY!<br/>
Aint no bunyips eating me today!"

So Diggity John, who lived in a billabong, now he understood. <br/> People were scared of him, 'cause he was a bunyip, <br/>and bunyips just don't belong. 

Diggity John, who lived in a billabong, sat and began to cry. <br/>
A quokka came by, <br/>and asked:"Diggity John, who lives in a billabong, oh why, <br/>oh why do you cry? <br/>
I'm not scared of you, <br/>I'll be your friend, <br/>I'll be your mend,<br/> oh Diggity John, who lives in a billabong, <br/>please, do end your pry."

Diggity John, he lived in a billabong, he wasn't alone, <br/>he would no longer moan, <br/>his friend the quokka did never groan.

 THE END.

