#+BEGIN_COMMENT
.. title: Unforgettable Mega Chilli
.. slug: unforgettable-mega-chilli
.. date: 2009-01-09 08:03:34 UTC+11:00
.. tags: chilli, food, cooking
.. link: 
.. description: Mike's Unforgettable Mega Chilli, first posted as a
Facebook Note, Jan 4 2009
.. type: text
#+END_COMMENT



*This is my own 2009 recipe, based on traditional Texan chilli with some*
*other ideas from Mexican chillies I've enjoyed. Texans will complain*
*that it's not chilli if it has beans, so they're optional.*

Ingredients:
----

  - 1 to 1.5kg **lean beef**, coarsely ground (minced) or small cubes
  - 1 **onion**, chopped
  - 1 tablespoon **olive oil** (or light cooking oil)
  - 5 cups **water**
  - 1 can (420g) **mixed beans** in liquid (or, 1 more cup water)
  - 2 **bay leaves**
  - 1 sprig **rosemary**, leaves left on
  - 1 teaspoon **oregano leaves**, crushed (just pluck and bash fresh)
  - 8 to 10 cloves **garlic**, minced

  - 1/2 cup **fresh chopped chillies** medium to hot
  - 1/2 tablespoon **chilli powder**
  - 2 tablespoons **dried chilli flakes**
  - 1 teaspoon **ground cumin**
  - 2 teaspoons **ground cayenne pepper** (or 3 if you dare)
  - 3 tablespoons **smoked paprika**

  - 1/2 teaspoon **black pepper**
  - 1 tablespoon **salt**
  - 1 tablespoon **sugar**

  - 3 tablespoons **flour** (plain wheat)
  - 6 tablespoons **cornflour** ("cornmeal")
  - **cold water**

Preparation:
----

In a 5-6 litre saucepan or a large dutch oven, brown beef with onion
in oil. Add water, beans (or more water), bay leaves, rosemary sprig
(yep, that's the *unforgettable* part),
oregano, garlic, chillies and spices, salt and sugar. Simmer, covered, 2
hours. Cool. You can serve it now but it's got some *serious*
kick. Refrigerate or leave covered on the bench overnight so that
flavors will combine and mellow.

Skim off the solidified fat. Reheat.

If too much liquid, combine flour, cornflour and a little cold water
to make a smooth paste.  Add paste mixture to chilli. Cook, stirring,
for about 6 to 8 minutes longer.

Remove bay leaves and rosemary twig before serving. Add more chilli for
extra kick if too mild.

**Serves 6.**

Variants?
----

Don't like the idea of rosemary in your chilli? That's okay, leave it out. Or better yet, substitute something else! Maybe ginger? Could whisky be made to work?

If you come up with something rad', then definitely you
should
[check out my project in GitLab](https://gitlab.com/milohax/mega-chilli/-/blob/master/recipe.md),
Fork it, make a branch and a Merge Request.

Recipes have always been Free/OpenSource...
