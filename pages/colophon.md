#+BEGIN_COMMENT
.. title: Colophon
.. slug: colophon
.. date: 2020-12-11T21:26+1100
.. tags: about, meta
.. link:
.. description: Some information about the site.
.. type: text
#+END_COMMENT

# Introductions

My name is Mike Lockhart, and this is my web log.  I am a [support engineer](https://about.gitlab.com/handbook/engineering/readmes/mike-lockhart/) for a well known Open Source web dev-ops company.  I do mainly Linux/Docker and [GitLab](http://about.gitlab.com) work, and dabble in some other languages and projects in my [Copious Free Time](/jargon/html/C/copious-free-time.html).

I'm also married since 1998 and we are parents to three boys born in 2005, 2010, and 2012.  We live in [Tasmania](http://discovertasmania.com.au), where my wife and I grew up, returning here from New South Wales in late 2013.  I feel grateful to be living here, with family close at hand and an awesome, friendly and welcoming community around us.

On the Net, I go by the handle *sinewalker*, and while after more than 20 years I think I've outgrown it, it's still a [fairly unique username](https://duckduckgo.com/?q=sinewalker) in most places.  A newer handle I've adopted is *milohax*.


## What am I into?

I have eclectic tastes, you can read [a list that I maintain](/pg/preferences.html), if it interests you.

This web
----

> *On this web site you will find posts, pages, pictures and code that are in connection with my interests.  I try to keep the stuff here as general as possible &mdash; any work-specific writings live on a [company website](https://gitlab.com/mlockhart/lab), or behind a company firewall. This means that what you see and read here is (or *was*) my own opinion and does not necessarily reflect the opinions or directions of my employers (past, present, or future)*.

This is the forth iteration of my blog. The [archive](/archive/archive.html) has old posts that I've migrated from [blogger](http://sinewalker.blogspot.com.au/) and [wordpress](https://sinewalker.wordpress.com/), though those are still online to be read in their original form. In late 2020 I migrated from [GitHub Pages](https://sinewalker.github.io/blog) to GitLab Pages with Continuous Integration and Deployment.

A major reason for building my blog with a static web page generator is so that I can have fine control over type-setting.  While this site is mainly a personal blog (so it's informal), I use notational conventions throughout.  These are common enough in online technical writings for the computer field, but weirdly not well supported by systems such as Blogger or Wordpress (or [Matrix](https://matrix.squiz.net)) which you must fight against to follow:

 * computer keywords are typed inline `like this`,
 * terms and variables are italicised *like that*, and
 * code blocks use *Colour Syntax Highlightng*, for the posts since late 2015.

You will notice that the English on this site follows Commonwealth conventions of grammar and spelling.  That's a natural consequence of where I live.  You will also notice frequent spelling errors which are *not* attributable to Commonwealth English &mdash; that's a consequence of [Rule 9](/pg/4-bit-rules.html).  I've also adopted the [Oxford Comma](https://en.wikipedia.org/wiki/Serial_comma) for its clarity and consistency.

Contacting me
----

This is not a "web 2.0" site: [there are no comments enabled here](/blog/2015/4-bit-rules-of-computing-part-2.html).  No-one comments anyway, and it adds a lot to the site load time and bandwidth.  There's not much point in costing visitors that overhead for something which isn't used.

Instead you should feel free to toot me [@milohax@hachyderm.io](https://hachyderm.io/@milohax), follow me [on GitHub](https://github.com/sinewalker), or [GitLab](https://gitlab.com/milohax), or by [e-mail](mailto:sinewalker@gmail.com), to share your thoughts.  Sensitive data may be emailed to me using my public PGP key with fingerprint [3CCA2E6EBCBE8795](https://keybase.io/sinewalker/key.asc).

While I do have a Facebook identity, please consider [following me on Keybase](https://keybase.io/sinewalker/) instead.  I prefer this method over the others, as it is the easiest way I know about to share things privately for free, with PGP cipher keys that you can validate for yourself by various Net handles.

How this web site is made
----

Posts are typed using the [Dvorak keyboard](/tags/dvorak.html) layout into [emacs](/tags/emacs.html) or VS Code on either an Apple PowerBook Mac, or a home-made computer running [openSUSE](http://www.opensuse.org). When possible I use an [ErgoDox EZ Glow](https://ergodox-ez.com/) with a [customized Dvorak layout](https://configure.ergodox-ez.com/ergodox-ez/layouts/YEOjn/latest/0).

Posts are generated from a mix of markup languages (typically [Markdown](http://daringfireball.net/projects/markdown/) or [iPython](http://ipython.org/) (Jupyter) [Notebooks](http://jupyter.org/)), using the [Nikola](http://getnikola.com) static web site and blog generator, and published to [GitLab Pages](https://pages.gitlab.io) on GitLab.com (via a 
[GitLab CI/CD pipeline](https://gitlab.com/milohax/milosophical-me/-/blob/src/.gitlab-ci.yml)).

The [source markup](https://gitlab.com/milohax/milosophical-me/-/blob/src/) files and Nikola [configuration](https://gitlab.com/milohax/milosophical-me/-/blob/src/conf.py) are stored in my [GitLab Project](https://gitlab.com/milohax/milosophical-me/).

The repository [wiki](https://gitlab.com/milohax/milosophical-me/-/wikis/home) and [issues log](https://gitlab.com/milohax/milosophical-me/-/issues) are where I keep notes about the site itself, rather than fill this site with
[meta-blog](/tags/meta.html) posts.

Copyleft
----

> *Prose and image content of all articles are copyright, licensed under a [Creative Commons Attribution-ShareAlike 4.0 License](http://creativecommons.org/licenses/by-nc-sa/4.0/), unless otherwise specified.  Code content are a mixture of [Open Source](https://opensource.org/)-compatible licenses, depending where the code ultimately originates or is intended to be used.  If codes are inline then I usually consider them to be “prose”, and if they're original to me then readers may infer that they are copyrighted and licensed in a spirit that is intended to be shared publicly, with attribution.*
