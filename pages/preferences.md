<!--
.. title: Preferences
.. slug: preferences
.. date: 2019-07-07 10:30:00 UTC+10:00
.. tags: 
.. category: 
.. link: 
.. description: What I am into.
.. type: text
-->

A quick summary of my inclinations.

## Likes:
  * Open Source Software
  * Open Data Formats
  * Open Architecture Computers
  * Open Crown hats
  * chili, coffee, whisky

## Dislikes:
  * opera (well, arias)
  * cruft
  * politics

## Hobbies:
  (what? I have 3 kids...)

  * walking
  * reading
  * hacking (old school)
  * dad stuff
  * robotics, electronics, maybe quad copters

## Alternatives:
  * emacs *vs* vi &rarr;  emacs
  * emacs *vs* VS Code &rarr;  code
  * tabs *vs* spaces &rarr; spaces
  * pull requests *vs* merge requests &rarr; merge requests
  * pirates *vs* ninjas &rarr; pirates
  * manual *vs* automatic transmission &rarr; manual (for internal combustion engines)
  * Star Trek *vs* Star Wars &rarr;  Star Wars
  * BSG *vs* Bab5 &rarr; BSG (re-imagined)
  * Windows *vs* macOS &rarr; macOS
  * Linux *vs* macOS &rarr; Linux (openSUSE/KDE)
  * iOS *vs* Android &rarr;  droid
  * GitHub *vs* GitLab &rarr; GitLab
  * Oreos Dunk *vs* Tim Tam Slam &rarr; Tim Tams
  * Hershy Bar Cookies `N' Creme *vs* Nestlé Milkybar &rarr; Milkybar
  * Pizza &rarr;   pineapple, *and* anchovies!
  * Coffee &rarr;  black
  * Tea &rarr;     Earl Grey, hot
  * Whiskey, Whisky, or Bourbon &rarr; Bourbon 
